package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.test.InstrumentationTestCase;

import junit.framework.TestCase;

import java.util.Vector;

public class PlanosCuidadosSQLiteTest extends InstrumentationTestCase {


    Context context;
    PlanosCuidadosSQLite planosCuidadosSQLite;

    public void setUp() throws Exception {
        super.setUp();

        context = getInstrumentation().getTargetContext();
        assertNotNull(context);
        planosCuidadosSQLite = new PlanosCuidadosSQLite(context);


    }

    public void testInsertarCuidados() throws Exception {
        planosCuidadosSQLite.eliminarBD();
        planosCuidadosSQLite.insertarCuidados(1,1,2,"Ayuda psicologica", "Neurologia");
        Vector v = planosCuidadosSQLite.obtener(1, 1);
        assertTrue(v.size() == 1);

        try{
           planosCuidadosSQLite.insertarCuidados(-1, 1, 2, "Ayuda", "Neurologia");
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }
    }

    public void testObtener() throws Exception {
        planosCuidadosSQLite.eliminarBD();
        planosCuidadosSQLite.insertarCuidados(1,1,2,"Ayuda psicologica", "Neurologia");

        Vector v = planosCuidadosSQLite.obtener(1, 1);
        assertTrue(v.size() == 1);

        v=planosCuidadosSQLite.obtener(3,2);
        assertTrue(v.size() == 0);

        try{
            planosCuidadosSQLite.obtener(-1,1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

    }

    public void testEliminarFila() throws Exception {
        planosCuidadosSQLite.eliminarBD();
        planosCuidadosSQLite.insertarCuidados(1,1,2,"Ayuda psicologica", "Neurologia");
        Vector v = planosCuidadosSQLite.obtener(1, 1);
        assertTrue(v.size() == 1);
        planosCuidadosSQLite.eliminarFila(1,1);
        v = planosCuidadosSQLite.obtener(1, 1);
        assertTrue(v.size() == 0);

        try{
            planosCuidadosSQLite.eliminarFila(-1,1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

    }
}