package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.test.InstrumentationTestCase;

import junit.framework.TestCase;

import java.util.Vector;

public class HistorialMedicoSQLiteTest extends InstrumentationTestCase {


    Context context;
    HistorialMedicoSQLite historialMedicoSQLite;

    public void setUp() throws Exception {
        super.setUp();

        context = getInstrumentation().getTargetContext();
        assertNotNull(context);
        historialMedicoSQLite = new HistorialMedicoSQLite(context);


    }

    public void testInsertarHistorialMedico() throws Exception {
        historialMedicoSQLite.eliminarBD();
        historialMedicoSQLite.insertarHistorialMedico(1,"neurologia", "2016-2-2", 1,1,1);
        Vector v = historialMedicoSQLite.obtener(1);
        assertTrue(v.size() == 1);

        v = historialMedicoSQLite.obtener(3);
        assertTrue(v.size() == 0);

        try {
            historialMedicoSQLite.insertarHistorialMedico(-1,"neurologia", "2016-2-2", 1,1,1 );
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }
        try {
            historialMedicoSQLite.insertarHistorialMedico(1,"neurologia", "2016-2", 1,1,1 );
        } catch (ExeceptionPropia e) {
            assertTrue("2".equals(e.getMessage().toString()));
        }
        historialMedicoSQLite.eliminarBD();

    }

    public void testObtener() throws Exception {
        historialMedicoSQLite.insertarHistorialMedico(1,"neurologia", "2016-2-2", 1,1,1);
        Vector v = historialMedicoSQLite.obtener(1);
        assertTrue(v.size() == 1);

        v = historialMedicoSQLite.obtener(3);
        assertTrue(v.size() == 0);

        try {
            historialMedicoSQLite.obtener(-1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }
        historialMedicoSQLite.eliminarBD();

    }


}