package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.test.AndroidTestCase;
import android.test.InstrumentationTestCase;
import android.util.Log;
import android.widget.ListView;

import junit.framework.TestCase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan Manuel on 01/02/2017.
 */


public class Integracion extends InstrumentationTestCase {


    Context context;
    InfoAltaSQLite infoAltaSQLite;
    HistorialSQLite historialIngresoSql;
    AdministracionMedSQLite administracionMedSQLite;
    MedicamentosSQLite medicamentosSQLite;
    AlertasSQLite  alertasSQLite;
    Metodos met;

    public void setUp() throws Exception {
        super.setUp();

        context = getInstrumentation().getContext();
        assertNotNull(context);
        historialIngresoSql = new HistorialSQLite(context);
        alertasSQLite = new AlertasSQLite(context);
        administracionMedSQLite = new AdministracionMedSQLite(context);
        medicamentosSQLite = new MedicamentosSQLite(context);
        met = new Metodos();

        metAuxiliares();

    }

        menu_app m = new menu_app();
        datos_ingreso d = new datos_ingreso();
        Lotesconpacientes lotes = new Lotesconpacientes();
        List<Ingreso> lista_ingresos;
        List<Paciente> lista_pacientes;
        Ingreso pacienteIngresado;
        ArrayList<Medicacion> lista_medic_final = new ArrayList<>();





        private void metAuxiliares() throws IOException, JSONException {
            lista_ingresos = met.cargar_ingresos(context);
            lista_pacientes = met.cargar_BDPacientes();
            pacienteIngresado = met.buscarIngreso(1, lista_ingresos);
        }

        public void testObtenerMedicacion() {
            // Lo insertamos por primera vez
            d.obtenerMedicacion();
            assertTrue(2 == lista_medic_final.size());
            // No lo volvemos a insertar
            d.obtenerMedicacion();
            assertTrue(2 == lista_medic_final.size());
        }

 /*       public void testObtenerMedicacionAlta() {
            // No hay medicacion alta
            ArrayList<Medicacion> altaMed = d.obtenerMedicacionAlta();
            if(altaMed == null) {
                assertTrue(true);
            }

            try {
                historialIngresoSql.insertarHistorial(pacienteIngresado.getId_i(), pacienteIngresado.getId(),
                        pacienteIngresado.getData(), 1, "1-1-0", "2016-2-5", "2016-3-5",
                        "nada", "alta", 2.0, "oral");
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
            }

            altaMed = d.obtenerMedicacionAlta();
            assertTrue(1 == altaMed.size());

        }

        public void testComprobarAlerta() {
            int i = d.comprobar_alertas();
            assertTrue(0 == i);

            try {
                alertasSQLite.insertarAlerta(1,"Paracetamol","nuevo med");
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
            }
            i = d.comprobar_alertas();
            assertTrue(1 == i);

            try {
                alertasSQLite.insertarAlerta(1,"Paracetamol","alta");
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
            }
            i = d.comprobar_alertas();
            assertTrue(2 == i);
        }

        public void testBuscarPaciente() throws ExeceptionPropia {
            String nombre_med = "Paracetamol";
            int id_lote = 1;
            int i = lotes.buscarNHCLote();
            assertTrue(i == 0);

            administracionMedSQLite.insertarAdmin(1,1,1,1,"2016-10-2");
            medicamentosSQLite.insertarMedicamento(1,1,"Paracetamol",2.0,"oral","2018-2-2");

            i = lotes.buscarNHCLote();
            assertTrue(i == 1);
        }

        public void testPrescribirMedicamento() throws ExeceptionPropia {

            // medicacion no existe
            d.pres_medicacion(pacienteIngresado.getId(), "Paracetamol", "2.0", "1-0-1", "oral", "2015-5-5", "2016-1-1",
                    "nada", "ingreso");
            // medicacion existe
            medicamentosSQLite.insertarMedicamento(1,1,"Paracetamol",2.0,"oral","2018-2-2");
            d.pres_medicacion(pacienteIngresado.getId(), "Paracetamol", "2.0", "1-0-1", "oral", "2015-5-5", "2016-1-1",
                    "nada", "ingreso");

            // no existen medicamentos
            medicamentosSQLite.insertarUsado(1,1,"Paracetamol", 2.0, "oral", "2018-2-2");
            int i = d.pres_medicacion(pacienteIngresado.getId(), "Paracetamol", "2.0", "1-0-1", "oral", "2015-5-5", "2016-1-1",
                    "nada", "ingreso");
            assertTrue(0 == i);
        }
*/

}
