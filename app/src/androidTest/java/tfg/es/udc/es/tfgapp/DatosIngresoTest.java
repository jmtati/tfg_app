package tfg.es.udc.es.tfgapp;

import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;

/**
 * Created by Juan Manuel on 07/02/2017.
 */
public class DatosIngresoTest extends ActivityInstrumentationTestCase2<datos_ingreso> {
    datos_ingreso datosI;

    public DatosIngresoTest() {
        super(datos_ingreso.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        datosI = getActivity();
        datosI.tipoUser = 1;
        datosI.id = "1";
    }

    public void testObtenerMedicacion() {
        Instrumentation.ActivityMonitor am = getInstrumentation().addMonitor(datos_ingreso.class.getName(), null, true);
        am.waitForActivity();
        assertEquals(1, am.getHits());

    }

}