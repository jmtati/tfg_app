package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.test.InstrumentationTestCase;

import junit.framework.TestCase;

import java.util.Vector;

public class MedicamentosSQLiteTest extends InstrumentationTestCase {


    Context context;
    MedicamentosSQLite medicamentosSQLite;

    public void setUp() throws Exception {
        super.setUp();

        context = getInstrumentation().getTargetContext();
        assertNotNull(context);
        medicamentosSQLite = new MedicamentosSQLite(context);


    }

    public void testInsertarMedicamento() throws Exception {
        medicamentosSQLite.eliminarBD();
        medicamentosSQLite.insertarMedicamento(1,1,"Paracetamol", 2.0, "oral", "2017-9-9");
        medicamentosSQLite.insertarMedicamento(2,1,"Neurofren", 2.0, "oral", "2017-9-3");

        Vector v = medicamentosSQLite.obtener(1);
        assertTrue(1 == v.size());
        v = medicamentosSQLite.obtener(2);
        assertTrue(1 == v.size());

         try { // id negativo
             medicamentosSQLite.insertarMedicamento(-1,1,"Neurofren", 2.0, "oral", "2017-9-3");
         } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
         }

        try { // fechas
            medicamentosSQLite.insertarMedicamento(5,1,"Neurofren", 2.0, "oral", "2017-9");
        } catch (ExeceptionPropia e) {
            assertTrue("2".equals(e.getMessage().toString()));
        }

        try { // vias
            medicamentosSQLite.insertarMedicamento(5,1,"Neurofren", 2.0, "nade na", "2017-9-21");
        } catch (ExeceptionPropia e) {
            assertTrue("5".equals(e.getMessage().toString()));
        }




    }

    public void testInsertarUsado() throws Exception {
       // medicamentosSQLite.insertarMedicamento(1,1,"Paracetamol", 2.0, "oral", "2017-9-9");
        //medicamentosSQLite.insertarMedicamento(2,1,"Neurofren", 2.0, "oral", "2017-9-3");

        medicamentosSQLite.insertarUsado(1, 1, "Paracetamol", 2.0, "oral", "2017-9-9");


        Vector v = medicamentosSQLite.obtener(1);
        assertTrue(1 == v.size());


        try { // id negativo
            medicamentosSQLite.insertarUsado(-1, 1, "Neurofren", 2.0, "oral", "2017-9-3");
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

        try { // fechas
            medicamentosSQLite.insertarUsado(5, 1, "Neurofren", 2.0, "oral", "2017-9");
        } catch (ExeceptionPropia e) {
            assertTrue("2".equals(e.getMessage().toString()));
        }

        try { // vias
            medicamentosSQLite.insertarUsado(5, 1, "Neurofren", 2.0, "nade na", "2017-9-21");
        } catch (ExeceptionPropia e) {
            assertTrue("5".equals(e.getMessage().toString()));
        }

    }

    public void testObtenerInfo() throws Exception {
        //medicamentosSQLite.insertarMedicamento(1,1,"Paracetamol", 2.0, "oral", "2017-9-9");
        //medicamentosSQLite.insertarMedicamento(2,1,"Neurofren", 2.0, "oral", "2017-9-3");

        Vector v = medicamentosSQLite.obtenerInfo(1);
        assertTrue(v.size() == 1);

        try { // id negativo
            medicamentosSQLite.obtenerInfo(-1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

    }

    public void testObtener() throws Exception {

        // medicamentosSQLite.insertarMedicamento(1,1,"Paracetamol", 2.0, "oral", "2017-9-9");
        //medicamentosSQLite.insertarMedicamento(2,1,"Neurofren", 2.0, "oral", "2017-9-3");

        Vector v = medicamentosSQLite.obtener(1);
        assertTrue(v.size() == 1);



        v = medicamentosSQLite.obtener(5);
        assertTrue(0 == v.size());

        try { // id negativo
            medicamentosSQLite.obtener(-1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }


    }

    public void testObtenerInfowName() throws Exception {
        //medicamentosSQLite.insertarMedicamento(1,1,"Paracetamol", 2.0, "oral", "2017-9-9");
        //medicamentosSQLite.insertarMedicamento(2,1,"Neurofren", 2.0, "oral", "2017-9-3");

        Vector v = medicamentosSQLite.obtenerInfowName("Paracetamol");
        assertTrue(v.size() == 1);


        v = medicamentosSQLite.obtenerInfowName("Avestruz");
        assertTrue(0 == v.size());
    }

    public void testObtenerId() throws Exception {
        //medicamentosSQLite.insertarMedicamento(1,1,"Paracetamol", 2.0, "oral", "2017-9-9");
        //medicamentosSQLite.insertarMedicamento(2,1,"Neurofren", 2.0, "oral", "2017-9-3");

        Vector v = medicamentosSQLite.obtenerInfowName("Paracetamol");
        assertTrue(v.size() == 1);


        v = medicamentosSQLite.obtenerInfowName("Avestruz");
        assertTrue(0 == v.size());

    }
}