package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.test.InstrumentationTestCase;

import junit.framework.TestCase;

import java.util.Vector;

public class InfoAltaSQLiteTest extends InstrumentationTestCase {


    Context context;
    InfoAltaSQLite infoAltaSQLite;

    public void setUp() throws Exception {
        super.setUp();

        context = getInstrumentation().getTargetContext();
        assertNotNull(context);
        infoAltaSQLite = new InfoAltaSQLite(context);


    }

    InformeAlta infoAlta = new InformeAlta(1, "2015-2-2", "2015-3-2", "nada", 2, 3,4);
    InformeAlta infoAlta2 = new InformeAlta(2, "2016-3-3", "2016-3-7", "nada", 2, 3,4);
    InformeAlta infoAltaN = new InformeAlta(-1, "2015-2-2", "2015-3-2", "nada", 2, 3,4);
    InformeAlta infoAltaF = new InformeAlta(1, "20152", "2015-3-2", "nada", 2, 3,4);

    public void testInsertarAlta() throws Exception {
        infoAltaSQLite.insertarAlta(infoAlta);
        Vector v = infoAltaSQLite.obtenerAlta(1, 1);
        assertTrue(v.size() == 1);

        try {
            infoAltaSQLite.insertarAlta(infoAltaN);
        }catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }
        try {
            infoAltaSQLite.insertarAlta(infoAltaF);
        }catch (ExeceptionPropia e) {
            assertTrue("2".equals(e.getMessage().toString()));
        }
        //infoAltaSQLite.eliminarBD();
    }

    public void testObtenerAlta() throws Exception {
        infoAltaSQLite.insertarAlta(infoAlta);
        infoAltaSQLite.insertarAlta(infoAlta2);

        Vector v = infoAltaSQLite.obtenerAlta(1, 1);
        assertTrue(v.size() == 1);

        try { // id negativo
            infoAltaSQLite.obtenerAlta(-1, 1);
        } catch (ExeceptionPropia e) {

        }

    }


    private boolean comprobarcionFechas(String fecha) {
        String[] c = fecha.split("-");
        if (c.length == 3) {
            return true;
        } else {
            return false;
        }
    }
}