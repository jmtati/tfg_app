package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.test.InstrumentationTestCase;

import junit.framework.TestCase;

import java.util.Vector;

public class HistorialSQLiteTest extends InstrumentationTestCase {


    Context context;
    HistorialSQLite histSql;

    public void setUp() throws Exception {
        super.setUp();

        context = getInstrumentation().getTargetContext();
        assertNotNull(context);
        histSql = new HistorialSQLite(context);


    }

    public void testObtenerMedicacion() throws ExeceptionPropia {
        histSql.eliminarDatos();
        histSql.insertarHistorial(1,1,"2015-2-2",1,"1-1-1","2015-2-5", "2015-2-8",
                "nada", "ingreso", 2.0, "subcutanea");
        histSql.insertarHistorial(1,1,"2015-2-2",1,"1-1-1","2015-2-15", "2015-2-28",
                "nada", "alta", 2.0, "oral");

        Vector v = histSql.obtenerMedicacion(1, 1, "2015-2-2");
        assertTrue(2 == v.size());

        v = histSql.obtenerMedicacion(2, 2, "2015-2-2");
        assertTrue(0 == v.size());

        try { // id negativo
            v = histSql.obtenerMedicacion(-1, 1, "2016-2-2");
        } catch (ExeceptionPropia e ) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

        try { // fecha incorrecta
            v = histSql.obtenerMedicacion(1, 1, "2016-");
        } catch (ExeceptionPropia e ) {
            assertTrue("2".equals(e.getMessage().toString()));
        }
        histSql.eliminarDatos();

    }

    public void testObtenerHistorial() throws Exception {
        histSql.eliminarDatos();

        histSql.insertarHistorial(1,1,"2015-2-2",1,"1-1-1","2015-2-5", "2015-2-8",
                "nada", "ingreso", 2.0, "subcutanea");
        histSql.insertarHistorial(1,1,"2015-2-4",1,"1-1-1","2015-2-15", "2015-2-28",
                "nada", "alta", 2.0, "oral");
        Vector v = histSql.obtenerHistorial(1,1,"todos");
        assertTrue(v.size() == 2);

        v =  histSql.obtenerHistorial(1,1,"2015-2-2");
        assertTrue(v.size() == 1);

        try { // id negativo
            histSql.obtenerMedicacion(-1,1,"2015-2-3");
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

        try { // fecha erronea
            histSql.obtenerMedicacion(1,1,"2015-3");
        } catch (ExeceptionPropia e) {
            assertTrue("2".equals(e.getMessage().toString()));
        }
        //histSql.eliminarBD();
        histSql.eliminarDatos();

    }

    public void testObtenerMedicacionAlta() throws Exception {
        histSql.eliminarDatos();

        histSql.insertarHistorial(1,1,"2015-2-2",1,"1-1-1","2015-2-5", "2015-2-8",
                "nada", "ingreso", 2.0, "subcutanea");
        histSql.insertarHistorial(1,1,"2015-2-4",1,"1-1-1","2015-2-15", "2015-2-28",
                "nada", "alta", 2.0, "oral");

        Vector v = histSql.obtenerMedicacionAlta(1);
        assertTrue(1 == v.size());

        try { //id negativo
            v = histSql.obtenerMedicacionAlta(-1);
        } catch (ExeceptionPropia e){
            assertTrue("1".equals(e.getMessage().toString()));
        }
        //histSql.eliminarBD();
        histSql.eliminarDatos();


    }

    public void testInsertarHistorial() throws Exception {
        histSql.eliminarDatos();

        histSql.insertarHistorial(1,1,"2015-2-2",1,"1-1-1","2015-2-5", "2015-2-8",
                "nada", "ingreso", 2.0, "subcutanea");
        histSql.insertarHistorial(1,1,"2015-2-2",1,"1-1-1","2015-2-15", "2015-2-28",
                "nada", "alta", 2.0, "oral");

        try { // Error en identificadores
            histSql.insertarHistorial(1,1,"2015-2-2",1,"1-1-1","2015-2-15", "2015-2-28",
                    "nada", "alta", 2.0, "oral");
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

        try { // Error en fechas
            histSql.insertarHistorial(1,1,"2015-2",1,"1-1-1","2015-2-15", "2015-2-28",
                    "nada", "alta", 2.0, "oral");
        } catch (ExeceptionPropia e) {
            assertTrue("2".equals(e.getMessage().toString()));
        }

        try { // Error en tipo
            histSql.insertarHistorial(1,1,"2015-2-2",1,"1-1-1","2015-2-15", "2015-2-28",
                    "nada", "malhecho", 2.0, "oral");
        } catch (ExeceptionPropia e) {
            assertTrue("4".equals(e.getMessage().toString()));
        }

        try { // Error en vias
            histSql.insertarHistorial(1,1,"2015-2-2",1,"1-1-1","2015-2-15", "2015-2-28",
                    "nada", "alta", 2.0, "noexiste");
        } catch (ExeceptionPropia e) {
            assertTrue("5".equals(e.getMessage().toString()));
        }
        //histSql.eliminarBD();
        histSql.eliminarDatos();



    }

}