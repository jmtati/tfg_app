package tfg.es.udc.es.tfgapp;

import android.app.Application;

import junit.framework.TestCase;


import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.test.InstrumentationTestCase;

import java.util.Vector;

public class AlertasSQLiteTest extends InstrumentationTestCase {


    Context context;
    AlertasSQLite alertasSQLite;

    public void setUp() throws Exception {
        super.setUp();

        context = getInstrumentation().getTargetContext();
        assertNotNull(context);
        alertasSQLite = new AlertasSQLite(context);


    }
    int nhc = 1;
    String motivo = "nuevo med";
    String name_med = "Paracetamol";
    String error = "DATO INVALIDO";



    public void testInsertarAlerta() throws ExeceptionPropia {
        alertasSQLite.eliminarBD();
        // Insertamos correctamente
        alertasSQLite.insertarAlerta(nhc, name_med, motivo);
        Vector v = alertasSQLite.obtener(nhc);
        assertTrue(v.size() == 1);

        try {
            // Insertamos dato erroneo nhc
            alertasSQLite.insertarAlerta(-1, name_med, motivo);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

        try {
            // Insertamos dato erroneo motivo
            alertasSQLite.insertarAlerta(1, name_med, "nada");
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }
        alertasSQLite.eliminarBD();

    }


    public void testObtenerAlerta() throws Exception{
        alertasSQLite.insertarAlerta(1, "Ibuprofeno", "alta");
        alertasSQLite.insertarAlerta(1, "Flumil", "nuevo med");

        Vector v = alertasSQLite.obtener(1);
        assertTrue(v.size() == 2);

        v = alertasSQLite.obtener(3);
        assertTrue(v.size() == 0);

        try {
            v = alertasSQLite.obtener(-1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }
        alertasSQLite.eliminarBD();
    }

    public void testObtenerMedAlert() throws ExeceptionPropia {
        alertasSQLite.insertarAlerta(1, "Ibuprofeno", "alta");
        alertasSQLite.insertarAlerta(1, "Flumil", "nuevo med");

        Vector v = alertasSQLite.obtenerMedAlerts(1);
        assertTrue(1 == v.size());

        v = alertasSQLite.obtenerMedAlerts(3);
        assertTrue(0 == v.size());

        try {
            v = alertasSQLite.obtenerMedAlerts(-1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }


    }

    public void testObtenerAltas() throws Exception {
        alertasSQLite.insertarAlerta(1, "Ibuprofeno", "alta");
        alertasSQLite.insertarAlerta(1, "Flumil", "alta");

        Vector v = alertasSQLite.obtenerAltas(1);
        assertTrue(2 == v.size());

        v = alertasSQLite.obtenerAltas(4);
        assertTrue(0 == v.size());

        try {
            v = alertasSQLite.obtenerAltas(-1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }
    }



}