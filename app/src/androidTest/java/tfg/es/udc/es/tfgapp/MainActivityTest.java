package tfg.es.udc.es.tfgapp;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.content.IntentFilter;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.ViewAsserts;
import android.view.View;
import android.widget.Button;

/**
 * Created by Juan Manuel on 07/02/2017.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    MainActivity mainActivity;
    View btn_acc, btn_nfc;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();
        btn_acc = mainActivity.findViewById(R.id.aceccs);
        btn_nfc = mainActivity.findViewById(R.id.btn_nfc);
    }

    public void testActionBarButtonsExists() {
        View mainActivityDecorView = mainActivity.getWindow().getDecorView();
        ViewAsserts.assertOnScreen(mainActivityDecorView, btn_acc);
        ViewAsserts.assertOnScreen(mainActivityDecorView, btn_nfc);
    }

   /* public void testAccesoMedico() {
        mainActivity.acceso("med", "1");

    }*/

    public void testAccesoError() {
        mainActivity.acceso("enf", "1"); // Toast
    }

    public void testActivity() {
        Instrumentation.ActivityMonitor am = getInstrumentation().addMonitor(menu_app.class.getName(), null, true);
        am.waitForActivityWithTimeout(10000);
        assertEquals(1, am.getHits());
    }

}
