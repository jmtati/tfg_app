package tfg.es.udc.es.tfgapp;

import android.app.Application;

import junit.framework.TestCase;


import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;
import android.test.InstrumentationTestCase;

import java.util.Vector;


/**
 * Created by Juan Manuel on 04/02/2017.
 */
public class AdministracionMedSQLiteTest extends InstrumentationTestCase {


    Context context;
    AdministracionMedSQLite adminMed;

    public void setUp() throws Exception {
        super.setUp();

        context = getInstrumentation().getTargetContext();
        assertNotNull(context);
        adminMed = new AdministracionMedSQLite(context);


    }

    public void testSomething() {

        assertEquals(true, true);
    }



    public void testInsertarAdmin() throws ExeceptionPropia {
        // Insertamos correctamente
        adminMed.insertarAdmin(1, 1, 1, 2, "2015-10-2");

        try {
            // Insertamos dato erroneo lote
            adminMed.insertarAdmin(1,1,2,-1,"2016-10-2");
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));

        }


        try {
            // Insertamos dato erroneo turno
            adminMed.insertarAdmin(1,1,5,1,"2016-10-2");
        } catch (ExeceptionPropia e) {
            assertTrue("3".equals(e.getMessage().toString()));
        }

        try {
            // Insertamos fecha incorrecta
                adminMed.insertarAdmin(1, 1, 2, 1, "2016-10");
        } catch (ExeceptionPropia e) {
            assertTrue("2".equals(e.getMessage().toString()));
        }
        adminMed.eliminarBD();


    }

    public void testObtener() throws ExeceptionPropia {
        // Insertamos correctamente
        adminMed.insertarAdmin(1, 1, 1, 2, "2015-10-2");
        adminMed.insertarAdmin(1, 2, 1, 1, "2015-10-3");
        adminMed.insertarAdmin(1, 3, 2, 1, "2015-10-15");

        Vector v = adminMed.obtener(1,1);
        assertTrue(2 == v.size());

        v = adminMed.obtener(1, 3);
        assertTrue(0 == v.size());

        //nhc negativo
        try {
            v = adminMed.obtener(-1, 1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

        // turno incorrecto
        try {
            v = adminMed.obtener(1, 5);
        } catch (ExeceptionPropia e) {
            assertTrue("3".equals(e.getMessage().toString()));
        }
        adminMed.eliminarBD();

    }

    public void testObtenerLote() throws ExeceptionPropia {
        adminMed.insertarAdmin(1, 1, 1, 2, "2015-10-2");
        adminMed.insertarAdmin(1, 2, 1, 1, "2015-10-3");
        adminMed.insertarAdmin(1, 3, 2, 1, "2015-10-15");

        // Lote y id qe existe
        Vector v = adminMed.obtenerLote(1,2);
        assertTrue(1 == v.size());

        // id existe lote no
        v = adminMed.obtenerLote(1,3);
        assertTrue(0 == v.size());

        try {
            // nhc en negativo
            v = adminMed.obtenerLote(-1, 1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }
        adminMed.eliminarBD();
    }

}
