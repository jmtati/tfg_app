package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.test.InstrumentationTestCase;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Vector;

public class MonitorizacionSQLiteTest extends InstrumentationTestCase {


    Context context;
    MonitorizacionSQLite monitorizacionSQLite;

    public void setUp() throws Exception {
        super.setUp();

        context = getInstrumentation().getTargetContext();
        assertNotNull(context);
        monitorizacionSQLite = new MonitorizacionSQLite(context);


    }

    public void testInsertarMonitorizacion() throws Exception {
        monitorizacionSQLite.eliminarBD();
        monitorizacionSQLite.insertarMonitorizacion(1,5,1,1,1);
        ArrayList v = monitorizacionSQLite.obtenerMonitorizacion(1);
        assertTrue(v.size() == 1);

        try {
            monitorizacionSQLite.insertarMonitorizacion(-1,1,1,1,1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

        try {
            monitorizacionSQLite.insertarMonitorizacion(1,-5,1,1,1);
        } catch (ExeceptionPropia e) {
            assertTrue("6".equals(e.getMessage().toString()));
        }

    }

    public void testObtenerMonitorizacion() throws Exception {

        monitorizacionSQLite.insertarMonitorizacion(1,5,1,1,1);
        ArrayList v = monitorizacionSQLite.obtenerMonitorizacion(1);
        assertTrue(v.size() == 1);

        v = monitorizacionSQLite.obtenerMonitorizacion(6);
        assertTrue(v.size() == 0);

        try {
            monitorizacionSQLite.obtenerMonitorizacion(-1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

    }

    public void testObtenerMonitorizacionHora() throws Exception {
        monitorizacionSQLite.insertarMonitorizacion(1,5,1,1,1);
        ArrayList v = monitorizacionSQLite.obtenerMonitorizacionHora(1, 5);
        assertTrue(v.size() == 1);

        v = monitorizacionSQLite.obtenerMonitorizacionHora(6, 5);
        assertTrue(v.size() == 0);

        try {
            monitorizacionSQLite.insertarMonitorizacion(-1,1,1,1,1);
        } catch (ExeceptionPropia e) {
            assertTrue("1".equals(e.getMessage().toString()));
        }

        try {
            monitorizacionSQLite.insertarMonitorizacion(1,-1,1,1,1);
        } catch (ExeceptionPropia e) {
            assertTrue("6".equals(e.getMessage().toString()));
        }

    }
}