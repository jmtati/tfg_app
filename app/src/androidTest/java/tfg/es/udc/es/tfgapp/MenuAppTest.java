package tfg.es.udc.es.tfgapp;

import android.app.Instrumentation;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ViewAsserts;
import android.view.View;

/**
 * Created by Juan Manuel on 07/02/2017.
 */
public class MenuAppTest extends ActivityInstrumentationTestCase2<menu_app> {
    menu_app MApp;

    public MenuAppTest() {
        super(menu_app.class);
    }
    @Override
    public void setUp() throws Exception {
        super.setUp();
        MApp = getActivity();
        //btn_acc = MApp.findViewById(R.id.btnScan);

    }

    /*public void testActionBarButtonsExists() {
        View mainActivityDecorView = MApp.getWindow().getDecorView();
        ViewAsserts.assertOnScreen(mainActivityDecorView, btn_acc);
    }*/

    public void testActivity() {
        Instrumentation.ActivityMonitor am = getInstrumentation().addMonitor(datos_ingreso.class.getName(), null, true);
        am.waitForActivityWithTimeout(10000);
        assertEquals(1, am.getHits());
    }

}
