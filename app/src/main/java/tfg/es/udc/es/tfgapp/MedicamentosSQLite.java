package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Vector;

/**
 * Created by Juan Manuel on 16/11/2016.
 */
public class MedicamentosSQLite extends SQLiteOpenHelper {
    //Métodos de SQLiteOpenHelper
    public MedicamentosSQLite(Context context) {
        super(context, "medicamentos", null, 1);
    }

    @Override public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE medicamentos ("+
                "id INTEGER, lote INTEGER, nombre VARCHAR, dosis DOUBLE, " +
                "vias VARCHAR, fecha_cad VARCHAR, usado INTEGER )");
        Log.d("MEDIC", "creamos BD medicamentos");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertarMedicamento(int id,  int lote, String nombre,
                                    double dosis, String vias, String fecha_cad) throws ExeceptionPropia{
        if(id >0 && lote>0 && dosis >0) {
            if(comprobacion_fecha(fecha_cad) ==1) {
                if(comprobacionVias(vias)) {
                    SQLiteDatabase db = getWritableDatabase();
                    db.execSQL("INSERT INTO medicamentos VALUES ( " + id + ",  " + lote +
                            ", '" + nombre + "', " + dosis + ", '" + vias + "', '" + fecha_cad + "', " + 0 + " ) ");
                    Log.d("MEDIC", "INSERTAMOS BD MEDICAMENTOS");
                    db.close();
                } else {
                    throw new ExeceptionPropia("5");
                }
            } else {
                throw new ExeceptionPropia("2");
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public void insertarUsado(int id,  int lote, String nombre,
                              double dosis, String vias, String fecha_cad) throws ExeceptionPropia{
        if(id >0 && lote>0 && dosis >0) {
            if(comprobacion_fecha(fecha_cad) == 1) {
                if(comprobacionVias(vias)) {
        Vector result = new Vector();
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " +
                "medicamentos WHERE id = " + id);

        db.close();

        SQLiteDatabase db2 = getWritableDatabase();
        db2.execSQL("INSERT INTO medicamentos VALUES ( "+id+",  "+lote+
                ", '"+nombre+"', "+dosis+", '"+vias+"', '" + fecha_cad + "', " + 1 + " ) ");
        Log.d("MEDIC", "INSERTAMOS BD MEDICAMENTOS");
        db2.close();
                } else {
                    throw new ExeceptionPropia("5");
                }
            } else {
                throw new ExeceptionPropia("2");
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }


    public Vector obtenerInfo(int idm) throws ExeceptionPropia {
        if(idm > 0) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT id, lote, nombre, dosis, vias, fecha_cad FROM " +
                    "medicamentos WHERE id = " + idm, null);
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getString(1) + "," + cursor.getString(2) + "," + cursor.getDouble(3) + "," + cursor.getString(4)
                        + "," + cursor.getString(5));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtener(int idm) throws ExeceptionPropia{
        if(idm>0) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT id, nombre, lote, fecha_cad FROM " +
                    "medicamentos WHERE id = " + idm + " ORDER BY lote DESC", null);
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getString(1) + "," + cursor.getString(2) + "," + cursor.getString(3));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtenerInfowName(String nombre) {
        Vector result = new Vector();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT id, lote, dosis, vias, fecha_cad, usado FROM " +
                "medicamentos WHERE nombre = '" + nombre +"'", null);
        while (cursor.moveToNext()){
            result.add(cursor.getInt(0)+","+cursor.getInt(1)+","+cursor.getDouble(2)+","+cursor.getString(3)+","+cursor.getString(4)+","+cursor.getInt(5));
        }
        cursor.close();
        db.close();
        return result;
    }

    public Vector obtenerId(String nombre) {
        Vector result = new Vector();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT id FROM " +
                "medicamentos WHERE nombre = '" + nombre +"'", null);
        while (cursor.moveToNext()){
            result.add(cursor.getInt(0));
        }
        cursor.close();
        db.close();
        return result;
    }




    public void eliminarBD() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("medicamentos",null,null);

    }

    private int comprobacion_fecha(String fecha_fin) {
        String[] c = fecha_fin.split("-");
        if(c.length==3) {
            int anho = Integer.parseInt(c[0].toString());
            int mes = Integer.parseInt(c[1].toString());
            int dia = Integer.parseInt(c[2].toString());
            Log.d("FECHAALTA", "1: " + anho + "," + mes + "," + dia);


            if (anho > 0 && mes > 0 && mes < 13 && dia > 0 && dia < 31) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }
    private boolean comprobacionVias(String vias) {
        if (vias.toLowerCase().equals("oral")) {
            return true;
        } else {
            if (vias.toLowerCase().equals("intravenosa")) {
                return true;
            } else {
                if (vias.toLowerCase().equals("subcutanea")) {
                    return true;
                } else {
                    if (vias.toLowerCase().equals("intramuscular")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    }

}
