package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Vector;

/**
 * Created by Juan Manuel on 20/07/2016.
 */
public class AlertasSQLite extends SQLiteOpenHelper {
    //Métodos de SQLiteOpenHelper
    public AlertasSQLite(Context context) {
        super(context, "alertas", null, 1);
    }

    @Override public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE alertas ("+
                "nhc INTEGER, id_med STRING, motivo VARCHAR )");
        Log.d("PUNTUACIONES", "creamos bd historial alertas");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertarAlerta(int nhc, String id_med, String motivo) throws ExeceptionPropia {
        if(nhc > 0) {
            if(motivo.equals("nuevo med") || motivo.equals("alta")) {
                SQLiteDatabase db = getWritableDatabase();
                db.execSQL("INSERT INTO alertas VALUES ( " + nhc + ", '" + id_med + "', '" + motivo + "' )");
                Log.d("ALERTAS 2", "Insertamos alerta " + nhc + " " + id_med + " " + motivo);
                db.close();
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public void eliminarFila(int nhc) throws ExeceptionPropia {
        if(nhc>0) {
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL("DELETE FROM alertas WHERE nhc = " + nhc);
            db.close();
        }else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtener(int nhc) throws ExeceptionPropia{
        if(nhc > 0) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT nhc, id_med FROM " +
                    "alertas WHERE nhc = " + nhc, null);
            Log.d("ALERTAS 2", "Obtenemos todas las alertas");
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getString(1));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw  new ExeceptionPropia("1");
        }

    }

    public Vector obtenerMedAlerts(int nhc) throws ExeceptionPropia{
        if(nhc > 0 ) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT nhc, id_med FROM " +
                    "alertas WHERE nhc = " + nhc + " AND motivo = 'nuevo med'", null);
            Log.d("ALERTAS 2", "Obtenemos todas las alertas");
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getString(1));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtenerAltas(int nhc) throws ExeceptionPropia{
        if(nhc > 0) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT nhc, id_med FROM " +
                    "alertas WHERE nhc = " + nhc + " AND motivo = 'alta'", null);
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getString(1));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw new ExeceptionPropia("1");
        }
    }






    public void eliminarBD() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("alertas",null,null);

    }
}
