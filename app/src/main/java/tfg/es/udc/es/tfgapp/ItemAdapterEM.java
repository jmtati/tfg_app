package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Juan Manuel on 26/04/2016.
 */
public class ItemAdapterEM extends BaseAdapter {

    private Context context;
    private List<Medicacion> items;
    int i = 0;

    public ItemAdapterEM(Context context, List<Medicacion> items) {
        this.context = context;
        this.items = items;
        for(int i = 0; i<items.size(); i++) {
            Log.d("LISTAITEMS", "Nombres " + items.get(i).getNombre());
        }
    }


    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_medicacionem ,parent, false);
        }

        // Set data into the view.
        TextView namemed = (TextView) rowView.findViewById(R.id.tv_nombreenfermera);
        TextView dosis = (TextView) rowView.findViewById(R.id.tv_tomado);
        TextView vias = (TextView) rowView.findViewById(R.id.tv_vias);
        TextView pautas = (TextView) rowView.findViewById(R.id.tv_pautas);
        if(i>1) {
            i = 0;
        }
        Log.d("FALLO", "Numero " + this.items.get(i) + ", " + position + ", " + i);

        Medicacion item = this.items.get(i);
        namemed.setText(item.getNombre() + " - ");
        dosis.setText(item.getDosis()+ " - ");
        vias.setText(item.getVias()+ " - ");
        pautas.setText(item.getPautas());
        i++;

        Log.d("LISTAMEDIC", "Numero TATI " + item.getNombre());

        return rowView;
    }
}
