package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Juan Manuel on 06/02/2017.
 */
public class EnfAdapter extends ArrayAdapter {
    private Context context;
    private ArrayList<Medicacion> datos;

    public EnfAdapter(Context context, ArrayList<Medicacion> datos) {
        super(context, R.layout.list_medicamento, datos);
        // Guardamos los parámetros en variables de clase.
        this.context = context;
        this.datos = datos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // En primer lugar "inflamos" una nueva vista, que será la que se
        // mostrará en la celda del ListView. Para ello primero creamos el
        // inflater, y después inflamos la vista.
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.list_medicamento, null);

        // Set data into the view.
        TextView namemed = (TextView) item.findViewById(R.id.tv_nombreenfermera);
        TextView dosis = (TextView) item.findViewById(R.id.tv_tomado);
        TextView vias = (TextView) item.findViewById(R.id.tv_vias);
        TextView pautas = (TextView) item.findViewById(R.id.tv_pautas);


        Medicacion datos_celda = this.datos.get(position);
        namemed.setText(datos_celda.getNombre());
        dosis.setText(datos_celda.getDosis()+ "");
        vias.setText(datos_celda.getVias());
        pautas.setText(datos_celda.getPautas());
        Log.d("MEDAUX", "Pasamos");

        // Devolvemos la vista para que se muestre en el ListView.
        return item;
    }
}
