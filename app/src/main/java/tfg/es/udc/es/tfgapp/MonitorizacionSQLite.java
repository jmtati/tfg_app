package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by Juan Manuel on 07/11/2016.
 */
public class MonitorizacionSQLite extends SQLiteOpenHelper {
    //Métodos de SQLiteOpenHelper
    public MonitorizacionSQLite(Context context) {
        super(context, "monitorizacion", null, 1);
    }

    @Override public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE monitorizacion ("+
                "nhc INTEGER, horas INTEGER, t INTEGER, ta INTEGER, " +
                "pa INTEGER )");
        Log.d("PUNTUACIONES", "creamos BD MONITORIZACION");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertarMonitorizacion(int nhc, int hora, int t, int ta, int pa) throws ExeceptionPropia {
        if(nhc > 0 && t > -1 && ta > -1 && pa > -1) {
            if(hora > -1 && hora < 24) {
                SQLiteDatabase db2 = getReadableDatabase();
                SQLiteDatabase db = getWritableDatabase();
                Cursor cursor = db2.rawQuery("SELECT nhc FROM " +
                        "monitorizacion WHERE nhc = " + nhc + " AND horas = " + hora + " ORDER BY horas ASC", null);
                if (cursor.getCount() != 0) {
                    db.execSQL("DELETE FROM monitorizacion WHERE horas = " + hora + " ");
                }
                db.execSQL("INSERT INTO monitorizacion VALUES ( " + nhc + ", '" + hora +
                        "', " + t + ", " + ta + ", " + pa + " )");
                db.close();
            } else {
                throw new ExeceptionPropia("6");
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public ArrayList<Monitorizacion> obtenerMonitorizacion(int nhc) throws ExeceptionPropia{
        if(nhc > 0) {
            ArrayList<Monitorizacion> result = new ArrayList<Monitorizacion>();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT nhc, horas, t, ta, pa FROM " +
                    "monitorizacion WHERE nhc = " + nhc + " ORDER BY horas ASC", null);
            while (cursor.moveToNext()) {
                //Log.d("PUNTUACIONES","Contenido" + cursor.getInt(1));
                result.add(new Monitorizacion(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4)));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public ArrayList<Monitorizacion> obtenerMonitorizacionHora(int nhc, int hora) throws ExeceptionPropia{
        if(nhc>0) {
            if(hora > -1 && hora < 24) {
                ArrayList<Monitorizacion> result = new ArrayList<Monitorizacion>();
                SQLiteDatabase db = getReadableDatabase();
                Cursor cursor = db.rawQuery("SELECT nhc, horas, t, ta, pa FROM " +
                        "monitorizacion WHERE nhc = " + nhc + " AND horas = " + hora, null);
                while (cursor.moveToNext()) {
                    //Log.d("PUNTUACIONES","Contenido" + cursor.getInt(1));
                    result.add(new Monitorizacion(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4)));
                }
                cursor.close();
                db.close();
                return result;
            } else {
                throw new ExeceptionPropia("6");
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public void eliminarFila(int nhc) throws ExeceptionPropia {
        if(nhc>0) {
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL("DELETE FROM monitorizacion WHERE nhc = " + nhc);
            db.close();
        }else {
            throw new ExeceptionPropia("1");
        }
    }



    public void eliminarBD() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("monitorizacion",null,null);

    }
}

