package tfg.es.udc.es.tfgapp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Juan Manuel on 14/03/2016.
 */
public class Medicacion {
    String nombre, pautas, fecha_ini, fecha_final, observaciones, tipo, fecha_cad;
    Double dosis;
    int lote;
    public enum Vias
    {
        ORAL, INTRAVENOSA, SUBCUTANEA, INTRAMUSCULAR
    }
    String vias;
    int tomada = 0;
    int id;
    int usado = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Medicacion(JSONObject objetoJSON) throws JSONException {
        nombre = objetoJSON.getString("nombre");
        dosis = objetoJSON.getDouble("dosis");
        pautas = objetoJSON.getString("pautas");
        /*switch (objetoJSON.getString("vias")) {
            case "oral":
                vias = Vias.ORAL;
                break;
            case "intravenosa":
                vias = Vias.INTRAVENOSA;
                break;
            case "subcutanea":
                vias = Vias.INTRAVENOSA;
                break;
            case "intramuscular":
                vias = Vias.SUBCUTANEA;
                break;
        }*/
        vias = objetoJSON.getString("vias");
        fecha_ini = objetoJSON.getString("fechadeinicio");
        fecha_final = objetoJSON.getString("fechafinal");
        observaciones = objetoJSON.getString("observaciones");
        tomada = 0;

    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFecha_cad() {
        return fecha_cad;
    }

    public void setFecha_cad(String fecha_cad) {
        this.fecha_cad = fecha_cad;
    }

    public int getUsado() {
        return usado;
    }

    public void setUsado(int usado) {
        this.usado = usado;
    }

    public Medicacion(int id, String nombre, Double dosis, int lote, String pautas, String vias, String fecha_inicio, String fecha_final, String observaciones, String tipo, String fecha_caducidad, int usado) {
        this.id = id;
        this.nombre = nombre;
        this.dosis = dosis;
        this.lote = lote;
        this.pautas = pautas;
        /*switch (vias) {
            case "oral":
                this.vias = Vias.ORAL;
                break;
            case "intravenosa":
                this.vias = Vias.INTRAVENOSA;
                break;
            case "subcutanea":
                this.vias = Vias.INTRAVENOSA;
                break;
            case "intramuscular":
                this.vias = Vias.SUBCUTANEA;
                break;
        }*/
        this.vias = vias;
        this.fecha_ini = fecha_inicio;
        this.fecha_final = fecha_final;
        this.fecha_cad = fecha_caducidad;

        this.observaciones = observaciones;
        this.tomada = 0;
        this.tipo = tipo;
        this.usado = usado;


    }

    public int getLote() {
        return lote;
    }

    public void setLote(int lote) {
        this.lote = lote;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getDosis() {
        return dosis;
    }

    public void setDosis(Double dosis) {
        this.dosis = dosis;
    }

    public String getPautas() {
        return pautas;
    }

    public void setPautas(String pautas) {
        this.pautas = pautas;
    }

    public String getVias() {
        return vias;
    }

    public void setVias(String vias) {
        this.vias = vias;
    }

    public String getFecha_ini() {
        return fecha_ini;
    }

    public void setFecha_ini(String fecha_ini) {
        this.fecha_ini = fecha_ini;
    }

    public String getFecha_final() {
        return fecha_final;
    }

    public void setFecha_final(String fecha_final) {
        this.fecha_final = fecha_final;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
}
