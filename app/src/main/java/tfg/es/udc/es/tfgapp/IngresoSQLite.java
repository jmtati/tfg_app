package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Vector;

/**
 * Created by Juan Manuel on 19/07/2016.
 */

public class IngresoSQLite extends SQLiteOpenHelper {
    //Métodos de SQLiteOpenHelper
    public IngresoSQLite(Context context) {
        super(context, "ingresos", null, 1);
    }

    @Override public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE ingresos ("+
                "id_i INTEGER, nhc INTEGER,"+
                "fecha VARCHAR, hora VARCHAR, procedencia VARCHAR, diagnostico VARCHAR, ta INTEGER, " +
                "fc INTEGER, t INTEGER, fr INTEGER, peso INTEGER, talla INTEGER)");
        Log.d("DATOS", "creamos bd ingreso");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertarIngreso(Ingreso ingreso) throws ExeceptionPropia{
        if(ingreso.getId() >0 && ingreso.getId_i()>0) {
            if(comprobacion_fecha(ingreso.getData())==1) {
                SQLiteDatabase db = getWritableDatabase();
                // GetID = nhc
                db.execSQL("INSERT INTO ingresos VALUES ( " + ingreso.getId_i() + ", " + ingreso.getId() + ", '" + ingreso.getData() + "', '" +
                        ingreso.getHora() + "', '" + ingreso.getPro() + "', '" + ingreso.getDiag() + "', " +
                        ingreso.getTa() + ", " + ingreso.getFc() + ", " + ingreso.getT() + ", " +
                        ingreso.getFr() + ", " + ingreso.getPeso() + ", " + ingreso.getTalla()
                        + " )");
                Log.d("INGRESOS", "Introducido ingreso: ");
                db.close();
            } else {
                throw new ExeceptionPropia("2");
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtenerIngreso(int id, int nhc) throws ExeceptionPropia{
        if(id >0 && nhc >0) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT fecha, hora, procedencia, diagnostico, ta, fc, t, fr, peso, talla FROM " +
                    "ingresos WHERE id_i = " + id + " AND nhc = " + nhc, null);
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getString(1) + "," + cursor.getString(2) + "," + cursor.getString(3) + "," + cursor.getString(4)
                        + "," + cursor.getString(5) + "," + cursor.getString(6) + "," + cursor.getString(7) + "," + cursor.getString(8) + "," + cursor.getString(9));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw new ExeceptionPropia("1");
        }
    }


    public void eliminarBD() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS ingresos");
        Log.d("DATOS", "Eliminar ingreso");
        //db.delete("ingresos",null,null);

    }

    private int comprobacion_fecha(String fecha_fin) {
        String[] c = fecha_fin.split("-");
        if(c.length==3) {
            int anho = Integer.parseInt(c[0].toString());
            int mes = Integer.parseInt(c[1].toString());
            int dia = Integer.parseInt(c[2].toString());
            Log.d("FECHAALTA", "1: " + anho + "," + mes + "," + dia);


            if (anho > 0 && mes > 0 && mes < 13 && dia > 0 && dia < 31) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }
}
