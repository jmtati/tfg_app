package tfg.es.udc.es.tfgapp;

/**
 * Created by Juan Manuel on 01/02/2017.
 */
public class ExeceptionPropia extends Exception {
    public ExeceptionPropia(String msg) {
        super(msg);
    }
}
