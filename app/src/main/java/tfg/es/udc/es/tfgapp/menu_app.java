package tfg.es.udc.es.tfgapp;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
// import zxing
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class menu_app extends ActionBarActivity implements View.OnClickListener {

    private Button btnScan, btnNfc;
    private int tipoUser;
    private List<Paciente> lista_pac;
    private NfcAdapter mNfcAdapter;
    Tag detectedTag;
    TextView txtType,txtSize,txtWrite,txtRead;
    NfcAdapter nfcAdapter;
    IntentFilter[] readTagFilters;
    PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_app);
        //String id = "1";
        tipoUser = getIntent().getExtras().getInt("tipo_usuario");
        Log.d("USUARIO", "Numero:" + tipoUser);
        /*Intent intent = new Intent(this, datos_ingreso.class);
        intent.putExtra("id_paciente", id);
        intent.putExtra("tipo_usuario", tipoUser);
        startActivity(intent);*/
        //------------------------
        btnScan = (Button) findViewById(R.id.btnScan);
        btnScan.setOnClickListener(this);

    }//end: onCreate

    private void Notify(String notificationTitle, String notificationMessage){
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        @SuppressWarnings("deprecation")

        Notification notification = new Notification(R.drawable.abc_ab_share_pack_holo_dark,"New Message", System.currentTimeMillis());
        Intent notificationIntent = new Intent(this,MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        notification.setLatestEventInfo(menu_app.this, notificationTitle,notificationMessage, pendingIntent);
        notificationManager.notify(9999, notification);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Si lo que leemos es QR
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if ( scanResult != null ) {
            String id = data.getStringExtra("SCAN_RESULT");
            //Paciente pacient = obtenerPaciente(Integer.parseInt(id));
            //Ingreso pacientingreso = lista_ingr; // Habria que buscar cual es el ingreso
            Intent intent = new Intent(this, datos_ingreso.class);
            intent.putExtra("id_paciente", id);
            // Para saber que mostrar
            intent.putExtra("tipo_usuario", tipoUser);
            startActivity(intent);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu_app, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        if(v == btnScan) {
            if(comprobamos_nfc() == 1) {
                // Dialogo para informar que acerque etiqueta NFC
                FragmentManager fragmentManager = getSupportFragmentManager();
                DialogoAlerta dialogo = new DialogoAlerta();
                dialogo.show(fragmentManager, "tag");

                Log.d("NFC", "Creamos diaalogo");
                nfcAdapter = NfcAdapter.getDefaultAdapter(this);
                detectedTag =getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);

                pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                        new Intent(this, getClass()).
                                addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

                IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
                IntentFilter filter2     = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
                readTagFilters = new IntentFilter[]{tagDetected,filter2};
                nfcAdapter.enableForegroundDispatch(this, pendingIntent, readTagFilters, null);


            } else {
                // Lanzamos qr
                IntentIntegrator.initiateScan(menu_app.this);
            }

        }
    }

    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        if(getIntent().getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)){
            detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            readFromTag(getIntent());

        }
    }

    public void readFromTag(Intent intent){

        Ndef ndef = Ndef.get(detectedTag);


        try{
            ndef.connect();
            String text = null;

            // txtType.setText(ndef.getType().toString());
            // txtSize.setText(String.valueOf(ndef.getMaxSize()));
            // txtWrite.setText(ndef.isWritable() ? "True" : "False");
            Parcelable[] messages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            if (messages != null) {
                NdefMessage[] ndefMessages = new NdefMessage[messages.length];
                for (int i = 0; i < messages.length; i++) {
                    ndefMessages[i] = (NdefMessage) messages[i];
                }
                NdefRecord record = ndefMessages[0].getRecords()[0];

                byte[] payload = record.getPayload();
                text = new String(payload);
                // txtRead.setText(text);


                ndef.close();

            }
            if(text.contains(".") || text.contains("-") || text.contains("/")) {
                Toast.makeText(menu_app.this, "ERROR EN LOS DATOS", Toast.LENGTH_SHORT).show();

            } else {
                if (tipoUser == 3) { // Medico
                    Intent intent_ingreso = new Intent(this, datos_ingreso.class);
                    Log.d("PACIENTE", "texto: " + text);
                    intent_ingreso.putExtra("id_paciente", text);
                    intent_ingreso.putExtra("tipo_usuario", tipoUser);
                    startActivity(intent_ingreso);
                    finish();
                } else {
                    if (tipoUser == 2) { // Enfermera
                        Intent intent_ingreso = new Intent(this, datos_ingreso_enf.class);
                        Log.d("PACIENTE", "texto: " + text);
                        intent_ingreso.putExtra("id_paciente", text);
                        intent_ingreso.putExtra("tipo_usuario", tipoUser);
                        startActivity(intent_ingreso);
                        finish();
                    } else {
                        Intent intent_ingreso = new Intent(this, datos_ingreso_aux.class);
                        Log.d("PACIENTE", "texto: " + text);
                        intent_ingreso.putExtra("id_paciente", text);
                        intent_ingreso.putExtra("tipo_usuario", tipoUser);
                        startActivity(intent_ingreso);
                        finish();
                    }
                }
            }
        }
        catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Cannot Read From Tag.", Toast.LENGTH_LONG).show();
        }
    }

    private int comprobamos_nfc() {
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (mNfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
            return 0;

        }

        if (!mNfcAdapter.isEnabled()) {
            Toast.makeText(this, "NFC is disabled.", Toast.LENGTH_SHORT).show();
            // Lanzariamos pantalla para conectar NFC
            return 1;
        } else {
            Toast.makeText(this, "Utilizaremos NFC.", Toast.LENGTH_SHORT).show();
            return 1;
        }
    }


}