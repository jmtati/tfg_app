package tfg.es.udc.es.tfgapp;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Juan Manuel on 26/04/2016.
 */
public class ItemAdapter extends BaseAdapter implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private Context context;
    private List<Medicacion> items;
    Button tvTitle;
    int tipoUser;
    String id_paciente;


    public ItemAdapter(Context context, List<Medicacion> items, int tipoUser, String id_paciente) {
        this.context = context;
        this.items = items;
        this.tipoUser = tipoUser;
        this.id_paciente = id_paciente;
        calcularTurno();
    }


    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_medicamento, parent, false);
        }

        // Set data into the view.
        TextView namemed = (TextView) rowView.findViewById(R.id.tv_nombreenfermera);
        TextView dosis = (TextView) rowView.findViewById(R.id.tv_tomado);
        TextView vias = (TextView) rowView.findViewById(R.id.tv_vias);
        TextView pautas = (TextView) rowView.findViewById(R.id.tv_pautas);


        Medicacion item = this.items.get(position);
        namemed.setText(item.getNombre() + " - ");
        dosis.setText(item.getDosis()+ " - ");
        vias.setText(item.getVias()+ " - ");
        pautas.setText(item.getPautas());
        Log.d("LISTAMEDIC", "Numero " + items.size());

        return rowView;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
            Log.d("PULSADO", "CAMBIAMOOOOOOS SI");
        } else {
            Log.d("PULSADO", "CAMBIAMOOOOOOS NO");
        }
    }



    @Override
    public void onClick(View v) {
        Intent i = new Intent(context, readNfcMed.class);
        i.putExtra("identificador", items.get(0).getId());
        i.putExtra("lote", items.get(0).getLote());
        i.putExtra("fecha_cad", items.get(0).getFecha_cad());
        // Falta esto
        i.putExtra("id_paciente", id_paciente);
        i.putExtra("tipo_usuario", tipoUser);
        int turno = calcularTurno();
        Log.d("TURNO", ": "+ id_paciente);
        i.putExtra("turno", turno);

        context.startActivity(i);

    }

    private int calcularTurno() {
        Calendar calendario = Calendar.getInstance();
        //Calendar calendario = new GregorianCalendar();
        int hora, minutos, segundos;
        hora =calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);
        Log.d("TURNO", "Horas: " + hora + " " + minutos);
        if(hora>4 && hora<12) {
            return 1;
        } else {
            if(hora>12 && hora<20) {
                return 2;
            } else {
                return 3;
            }
        }
    }
}
