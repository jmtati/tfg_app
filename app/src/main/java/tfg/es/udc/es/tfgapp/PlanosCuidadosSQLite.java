package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Vector;

/**
 * Created by Juan Manuel on 26/12/2016.
 */

public class PlanosCuidadosSQLite extends SQLiteOpenHelper {
    //Métodos de SQLiteOpenHelper
    public PlanosCuidadosSQLite(Context context) {
        super(context, "cuidados", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE cuidados (" +
                "nhc INTEGER, id_i INTEGER, codigo INTEGER, nombre VARCHAR, categoria VARCHAR " +
                ")");
        Log.d("HISMEDICO", " creamos BDHISTORIAL");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertarCuidados(int nhc, int id_i, int codigo, String nombre, String categoria) throws ExeceptionPropia{
        if(nhc > 0 && id_i > 0 && codigo >0) {
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL("INSERT INTO cuidados VALUES ( " + nhc + ", " + id_i + ", " + codigo +
                    ", '" + nombre + "', '" + categoria + "' )");
            Log.d("HISMEDICO", "INSERTAMOS BDHISTORIAL");
            db.close();
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtener(int nhc, int id_i) throws ExeceptionPropia{
        if(nhc > 0 && id_i > 0) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT nhc, codigo, nombre, categoria FROM " +
                    "cuidados WHERE nhc = " + nhc + " AND id_i = " + id_i, null);
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getString(1) + "," + cursor.getString(2) + "," + cursor.getString(3));
            }
            cursor.close();
            db.close();
            return result;
        }else {
            throw new ExeceptionPropia("1");
        }
    }

    public void eliminarFila(int nhc, int id_i) throws ExeceptionPropia {
        if(nhc>0 && id_i>0) {
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL("DELETE FROM cuidados WHERE nhc = " + nhc + " AND id_i = " + id_i);
            Log.d("HISMEDICO", "INSERTAMOS BDHISTORIAL");
            db.close();
        }else {
            throw new ExeceptionPropia("1");
        }
    }



    public void eliminarBD() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("cuidados", null, null);

    }

}
