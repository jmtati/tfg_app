package tfg.es.udc.es.tfgapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan Manuel on 17/07/2016.
 **/


public class Monitorizacion {

    int nhc;
    int turno;
    int t, ta,pa;


    public Monitorizacion(int nhc, int hora, int t, int ta, int pa)  {
        this.nhc = nhc;
        this.turno = hora;
        this.t = t;
        this.ta = ta;
        this.pa = pa;
      }

    public int getTurno() {
        return turno;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public int getTa() {
        return ta;
    }

    public void setTa(int ta) {
        this.ta = ta;
    }

    public int getPa() {
        return pa;
    }

    public void setPa(int pa) {
        this.pa = pa;
    }

    public int getNhc() {
        return nhc;
    }

    public void setNhc(int nhc) {
        this.nhc = nhc;
    }


}
