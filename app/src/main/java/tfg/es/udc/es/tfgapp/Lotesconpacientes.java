package tfg.es.udc.es.tfgapp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


public class Lotesconpacientes extends ActionBarActivity implements AdapterView.OnItemClickListener {

    ListView listView;
    int id_lote = -1;
    String nombre_med;
    AdministracionMedSQLite adminSql;
    MedicamentosSQLite medSql;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    class Datos {
        String nhc;
        String fecha;
        Datos(String f, String e) {
            nhc = f;
            fecha = e;
        }

        public String getNhc() {
            return nhc;
        }

        public void setNhc(String nhc) {
            this.nhc = nhc;
        }

        public String getFecha() {
            return fecha;
        }

        public void setFecha(String especialidad) {
            this.fecha = especialidad;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lotesconpacientes);
        nombre_med = getIntent().getExtras().getString("nombre_med");
        id_lote = getIntent().getExtras().getInt("lote_buscar");
        listView = (ListView) findViewById(R.id.lv_pacientslote);
        adminSql = new AdministracionMedSQLite(this);
        medSql = new MedicamentosSQLite(this);

        if(id_lote!=-1) {
            buscarNHCLote();
        }

    }

    public int buscarNHCLote() {
        Vector id = medSql.obtenerId(nombre_med);
        List<Datos> result = new ArrayList<>();
        if(id.size()>0) {
            result = new ArrayList<>();
            for(int m = 0;m<id.size();m++) {
                int id_medicamento = Integer.parseInt(id.get(m).toString());
                Log.d("BUSCARLOTE", "id: "+ id_medicamento);
                Vector valor = null;
                try {
                    valor = adminSql.obtenerLote(id_medicamento, id_lote);
                } catch (ExeceptionPropia execeptionPropia) {
                    execeptionPropia.printStackTrace();
                }
                for (int i = 0; i < valor.size(); i++) {
                    Log.d("BUSCARLOTE", "nombre: " + valor.get(i).toString());
                    String[] datos_valor = valor.get(i).toString().split(",");
                    if(datos_valor.length==2) {
                        result.add(new Datos(datos_valor[0], datos_valor[1]));
                    }

                }

            }
            if(result.size()>0) {

                ItemAdapterLotes adapter3 = new ItemAdapterLotes(this, result);
                listView.setAdapter(adapter3);
                listView.setOnItemClickListener(this);
                return 1;
            } else {
                Toast.makeText(this, "No existe ningun paciente que haya consumido ese lote", Toast.LENGTH_SHORT).show();
                return 0;
            }
        } else {
            Toast.makeText(this, "No existe el medicamento especificado en la base de datos.", Toast.LENGTH_SHORT).show();
            return 0;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lotesconpacientes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
