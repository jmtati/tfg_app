package tfg.es.udc.es.tfgapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Vector;


public class historialMedico_activity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    HistorialMedicoSQLite historial_medicoSql;
    MedicamentosSQLite med_sql;
    IngresoSQLite ingresoSql;
    HistorialSQLite historialSql;
    InfoAltaSQLite altaSql;
    int nhc = -1;
    ArrayList<HistorialDatos> neurologia;
    ArrayList<HistorialDatos> neumologia;
    ArrayList<HistorialDatos> cardiologia;
    ArrayList<HistorialDatos> med_general;
    ListView lv_neurologia, lv_neumologia, lv_cardiologia, lv_medgeneral;
    Dialog customDialog = null;

    class Datos {
        String fecha;
        String especialidad;
        Datos(String f, String e) {
            fecha = f;
            especialidad = e;
        }

        public String getFecha() {
            return fecha;
        }

        public void setFecha(String fecha) {
            this.fecha = fecha;
        }

        public String getEspecialidad() {
            return especialidad;
        }

        public void setEspecialidad(String especialidad) {
            this.especialidad = especialidad;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_medico_activity);
        /*lv_neurologia = (ListView) findViewById(R.id.lv_neurologia);
        lv_neumologia = (ListView) findViewById(R.id.lv_neumologia);*/
        lv_cardiologia = (ListView) findViewById(R.id.lv_cardiologia);
        //lv_medgeneral = (ListView) findViewById(R.id.lv_medicinageneral);

        // Creamos BD
        historial_medicoSql = new HistorialMedicoSQLite(this);
        med_sql = new MedicamentosSQLite(this);
        ingresoSql = new IngresoSQLite(this);
        historialSql = new HistorialSQLite(this);
        altaSql = new InfoAltaSQLite(this);
        nhc = getIntent().getExtras().getInt("nhc");
        obtenerHistorial(nhc);

        ArrayList<Long> fechas_neuro = new ArrayList<Long>();
        ArrayList<Long> fechas_neumo = new ArrayList<Long>();
        ArrayList<Datos> fechas_card = new ArrayList<Datos>();
        ArrayList<Long> fechas_medi_general = new ArrayList<Long>();
        /*for(int i = 0;i<neurologia.size();i++) {
            fechas_neuro.add(neurologia.get(i).getFecha());
        }
        for(int i = 0;i<neumologia.size();i++) {
            fechas_neumo.add(neumologia.get(i).getFecha());
        }

        for(int i = 0;i<med_general.size();i++) {
            fechas_medi_general.add(med_general.get(i).getFecha());
        }*/

        for(int i = 0;i<cardiologia.size();i++) {
            Datos d = new Datos(cardiologia.get(i).getFecha(), cardiologia.get(i).getEspecialidad());
            fechas_card.add(d);
        }

        ItemAdapterHist adapter3 = new ItemAdapterHist(this, fechas_card);
        lv_cardiologia.setAdapter(adapter3);
        lv_cardiologia.setOnItemClickListener(this);
        /*
        ArrayAdapter<Long> adapter = new ArrayAdapter<Long>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, fechas_neuro);
        lv_neurologia.setAdapter(adapter);
        lv_neurologia.setOnItemClickListener(this);
        ArrayAdapter<Long> adapter2 = new ArrayAdapter<Long>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, fechas_neumo);
        lv_neumologia.setAdapter(adapter2);
        lv_neumologia.setOnItemClickListener(this);
        ArrayAdapter<Long> adapter4 = new ArrayAdapter<Long>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, fechas_medi_general);
        lv_medgeneral.setAdapter(adapter4);
        lv_medgeneral.setOnItemClickListener(this);
        */

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_historial_medico_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void obtenerHistorial(int nhc) {
        Log.d("HISTORIAL", "Resultado:" + nhc);
        //historial_medicoSql.insertarHistorialMedico(nhc, "neurologia", 2, 1,1,1);
        //historial_medicoSql.insertarHistorialMedico(nhc, "neumologia", 5, 2,2,2);
        //historial_medicoSql.insertarHistorialMedico(nhc, "neumologia", 7, 3,3,3);
        //historial_medicoSql.insertarHistorialMedico(nhc, "neurologia", 6, 4,4,4);
        Vector resultado = null;
        try {
            resultado = historial_medicoSql.obtener(nhc);
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
        }
        Log.d("HISTORIAL", "Resultado2:" + resultado.size());

        ArrayList<HistorialDatos> hist = new ArrayList<HistorialDatos>();
        for(int i = 0;i<resultado.size();i++) {
            String[] a = resultado.get(i).toString().split(",");
            hist.add(new HistorialDatos(nhc, a[1], a[2], Integer.parseInt(a[3]), Integer.parseInt(a[4]), Integer.parseInt(a[5])));
        }
        Log.d("HISTORIAL", "Resultado3:" + hist.size());
        // Dividilo en especialidades: neurologia, cardiologia, neumologia y medicina general. Ordenado por fecha.
        neurologia = new ArrayList<HistorialDatos>();
        neumologia = new ArrayList<HistorialDatos>();
        cardiologia = new ArrayList<HistorialDatos>();
        med_general = new ArrayList<HistorialDatos>();
        cardiologia = hist;
        /*for(int i = 0;i<hist.size();i++) {
            //if(hist.get(i).getEspecialidad().equals("cardiologia")) {
                cardiologia.add(hist.get(i));
                Log.d("HISTORIAL", "Añadimos a cardiologia");
            /*} else {
                if(hist.get(i).getEspecialidad().equals("neumologia")) {
                    neumologia.add(hist.get(i));
                    Log.d("HISTORIAL", "Añadimos a neumologia " + hist.get(i).getFecha());
                } else {
                    if(hist.get(i).getEspecialidad().equals("neurologia")) {
                        neurologia.add(hist.get(i));
                        Log.d("HISTORIAL", "Añadimos a neurologia" +hist.get(i).getFecha());
                    } else {
                        med_general.add(hist.get(i));
                        Log.d("HISTORIAL", "Añadimos a medicina general" +hist.get(i).getFecha());
                    }
                }
            }
        }*/

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Log.d("HISTORIAL", "Obtenemos: " + position + " " + id);
        // Mostramos la informacion acerca de ingreso, historial y alta de la fecha seleccionada!!
       /* if(parent == lv_neurologia) {
            Log.d("HISTORIAL", "Obtenemos: " + position + " " + id + " " + neurologia.get(position).getId_i());
            Vector v_ingreso = ingresoSql.obtenerIngreso(neurologia.get(position).getId_i(),neurologia.get(position).getNhc());
            Vector v_historial = historialSql.obtenerHistorial(neurologia.get(position).getId_h(), neurologia.get(position).getNhc(), "todas");
            Vector v_alta = altaSql.obtenerAlta(neurologia.get(position).getId_a(), neurologia.get(position).getNhc());
            Log.d("HISTORIAL", "+" + v_ingreso.size() + " " + v_historial.size() + " " + v_alta.size());

        }
        if(parent == lv_neumologia) {
            Log.d("HISTORIAL", "Obtenemos2: " + position + " " + id);
            Log.d("HISTORIAL", "Obtenemos: " + position + " " + id + " " + neumologia.get(position).getId_i());
            Vector v_ingreso = ingresoSql.obtenerIngreso(neumologia.get(position).getId_i(), neumologia.get(position).getNhc());
            Vector v_historial = historialSql.obtenerHistorial(neumologia.get(position).getId_h(), neumologia.get(position).getNhc(), "todas");
            Vector v_alta = altaSql.obtenerAlta(neumologia.get(position).getId_a(), neumologia.get(position).getNhc());
            Log.d("HISTORIAL", "+" + v_ingreso.size() + " " + v_historial.size() + " " + v_alta.size());
        }*/
        if(parent == lv_cardiologia) {
            Log.d("HISTORIAL", "Obtenemos3: " + position + " " + id);
            Log.d("HISTORIAL", "Obtenemos: " + position + " " + id + " " + cardiologia.get(position).getId_i());
            Vector v_ingreso = null;
            try {
                v_ingreso = ingresoSql.obtenerIngreso(cardiologia.get(position).getId_i(), cardiologia.get(position).getNhc());
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
            }
            Vector v_historial = null;
            try {
                v_historial = historialSql.obtenerHistorial(cardiologia.get(position).getId_i(),cardiologia.get(position).getNhc(), "todos");
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
            }
            Vector v_alta = null;
            try {
                v_alta = altaSql.obtenerAlta(cardiologia.get(position).getId_a(), cardiologia.get(position).getNhc());
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
            }
            Log.d("HIST-DATOS", " " + cardiologia.get(position).getId_i() + " " + v_ingreso.size() + " " + v_historial.size() + " " + v_alta.size());
            creamos_dialogo(v_ingreso, v_historial, v_alta);
        }
        /*
        if(parent == lv_medgeneral) {
            Log.d("HISTORIAL", "Obtenemos4: " + position + " " + id);
            Log.d("HISTORIAL", "Obtenemos: " + position + " " + id + " " + med_general.get(position).getId_i());
            Vector v_ingreso = ingresoSql.obtenerIngreso(med_general.get(position).getId_i(), med_general.get(position).getNhc());
            Vector v_historial = historialSql.obtenerHistorial(med_general.get(position).getId_h(), med_general.get(position).getNhc(), "todas");
            Vector v_alta = altaSql.obtenerAlta(med_general.get(position).getId_a(), med_general.get(position).getNhc());
            Log.d("HISTORIAL", "+" + v_ingreso.size() + " " + v_historial.size() + " " + v_alta.size());
        }*/
    }

    private void creamos_dialogo(Vector ingreso, Vector historial, Vector alta) {
        /*AlertDialog.Builder dialog = new AlertDialog.Builder(historialMedico_activity.this);
        dialog.setTitle("Introduzca datos");
        Context context = historialMedico_activity.this;
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        dialog.setTitle("HISTORIAL MEDICO");

        final TextView in = new TextView(context);
        layout.addView(in);
        // fecha, hora, procedencia, diagnostico, ta, fc, t, fr, peso, talla
        String[] ingreso_datos = ingreso.get(0).toString().split(",");
        in.setText("INGRESO \n-Fecha: " + ingreso_datos[0]
        + "\n-Hora: " + ingreso_datos[1]
        + "\n-Procedencia: " + ingreso_datos[2]
        + "\n-Diagnostico: " + ingreso_datos[3]
        );

        final TextView hi = new TextView(context);
        layout.addView(hi);
        String texto = "\nHISTORIAL ";
        for(int m = 0; m<historial.size(); m++) {
            // id_medicamento, pautas, fecha_inicio, fecha_fin, observacions
            String[] hist_datos = historial.get(m).toString().split(",");
            texto = texto + "\n-Id Medicamento: " + hist_datos[0]
                    + "\n-Pautas: " + hist_datos[1]
                    + "\n-Fecha inicio: " + hist_datos[2]
                    + "\n-Fecha fin: " + hist_datos[3] + "\n";
        }
        hi.setText(texto);

        final TextView al = new TextView(context);
        layout.addView(al);
        // fecha_entrada, fecha_salida, recomendaciones, ta, t, pa
        String[] alta_datos = alta.get(0).toString().split(",");
        al.setText("ALTA \n-Fecha entrada: " + alta_datos[0]
                        + "\n-Fecha salida: " + alta_datos[1]
                        + "\n-Recomendaciones: " + alta_datos[2]
        );




        dialog.setView(layout);

        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {


            }
        });



        dialog.show();*/

        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.dialog_historial);

        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("DATOS CONSULTAS");

        final TextView datos_ingreso = (TextView) customDialog.findViewById(R.id.dialog_di);
        String[] ingreso_datos = ingreso.get(0).toString().split(",");
        datos_ingreso.setText("-Fecha: " + ingreso_datos[0]
                        + "\n-Hora: " + ingreso_datos[1]
                        + "\n-Procedencia: " + ingreso_datos[2]
                        + "\n-Diagnostico: " + ingreso_datos[3]
        );

        final TextView datos_hist = (TextView) customDialog.findViewById(R.id.dialog_dh);
        String texto = "";
        for(int m = 0; m<historial.size(); m++) {

            // id_medicamento, pautas, fecha_inicio, fecha_fin, observacions
            String[] hist_datos = historial.get(m).toString().split(",");
            Vector v = new Vector();
            try {
               v = med_sql.obtener(Integer.parseInt(hist_datos[0]));
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
            }
            if(v.size()>0) {
                String[] name = v.get(0).toString().split(",");
                texto = texto + "\n-Nombre: " + name[1]
                        + "\n-Pautas: " + hist_datos[1]
                        + "\n-Fecha inicio: " + hist_datos[2]
                        + "\n-Fecha fin: " + hist_datos[3] + "\n";
            }
        }
        datos_hist.setText(texto);

        final TextView datos_alta = (TextView) customDialog.findViewById(R.id.dialog_da);
        String[] alta_datos = alta.get(0).toString().split(",");
        datos_alta.setText("ALTA \n-Fecha entrada: " + alta_datos[0]
                        + "\n-Fecha salida: " + alta_datos[1]
                        + "\n-Recomendaciones: " + alta_datos[2]
        );



        //TextView contenido = (TextView) customDialog.findViewById(R.id.contenido);
        //contenido.setText("Mensaje con el contenido del dialog");

        ((Button) customDialog.findViewById(R.id.aceptar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();

            }
        });
        customDialog.show();

    }
}
