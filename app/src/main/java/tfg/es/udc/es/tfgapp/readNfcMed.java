package tfg.es.udc.es.tfgapp;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;


public class readNfcMed extends ActionBarActivity implements View.OnClickListener{

    int id, lote;
    TextView tv_med, tv_nfc;
    Button but_nfc;
    int lote_leido = -1, id_lote = -1;

    private NfcAdapter mNfcAdapter;
    Tag detectedTag;
    TextView txtType,txtSize,txtWrite,txtRead;
    NfcAdapter nfcAdapter;
    IntentFilter[] readTagFilters;
    PendingIntent pendingIntent;
    DialogoAlerta dialogo;
    Button b;
    AdministracionMedSQLite adminMed;
    int nhc;
    int turno;
    int tipoUser;
    LinearLayout ll;
    String fec_cad;
    Dialog customDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_nfc_med);
        adminMed = new AdministracionMedSQLite(this);
        nhc = (getIntent().getExtras().getInt("id_paciente"));
        tipoUser = getIntent().getExtras().getInt("tipo_usuario");
        turno = getIntent().getExtras().getInt("turno");
        id = getIntent().getExtras().getInt("identificador");
        fec_cad =  getIntent().getExtras().getString("fecha_cad");
        lote = getIntent().getExtras().getInt("lote");
        ll = (LinearLayout) findViewById(R.id.ll_nfc);
        Log.d("NFCMED", ":" + id + " " + lote);
        tv_med = (TextView) findViewById(R.id.tv_med);
        tv_med.setText("ID: " + id + " LOTE:" + lote);
        tv_nfc = (TextView) findViewById(R.id.tv_nfc);
        but_nfc = (Button) findViewById(R.id.but_nfcmed);
        but_nfc.setOnClickListener(this);

    }

    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        if(getIntent().getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)){
            detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            readFromTag(getIntent());

        }
    }

    public void readFromTag(Intent intent){

        Ndef ndef = Ndef.get(detectedTag);


        try{
            ndef.connect();
            String text = null;

            // txtType.setText(ndef.getType().toString());
            // txtSize.setText(String.valueOf(ndef.getMaxSize()));
            // txtWrite.setText(ndef.isWritable() ? "True" : "False");
            Parcelable[] messages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            if (messages != null) {
                NdefMessage[] ndefMessages = new NdefMessage[messages.length];
                for (int i = 0; i < messages.length; i++) {
                    ndefMessages[i] = (NdefMessage) messages[i];
                }
                NdefRecord record = ndefMessages[0].getRecords()[0];

                byte[] payload = record.getPayload();
                text = new String(payload);
                Log.d("NFCTATI", "Texto: " + text);
                // txtRead.setText(text);


                ndef.close();

            }
            String[] user = text.split("-");
            if(user.length == 2) {
                String id_leidoS = user[0];
                String fecha_caducidad_leida = user[1];
                //String fecha_caducidad_leida = user[2];
                id_lote = Integer.parseInt(id_leidoS);
                lote_leido = Integer.parseInt(fecha_caducidad_leida);
                Log.d("NFCTATI", "Texto 2: " + fecha_caducidad_leida);
                dialogo.dismiss();
                tv_nfc.setText("NFC: ID: " + id_lote + " LOTE: " + lote_leido);
                //comprobamos_compatibilidad(id_lote, lote_leido, fecha_caducidad_leida);
                comprobamos_compatibilidad(id_lote, lote_leido, fec_cad);
                Log.d("NFCTATI", "Texto 3:");


            } else {
                Toast.makeText(this, "ERROR EN LOS DATOS", Toast.LENGTH_SHORT).show();
            }

        }
        catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Cannot Read From Tag.", Toast.LENGTH_LONG).show();
        }
    }

    private int caducado(String fecha_caducidad) {
        //formato fecha yyyy-mm-dd
        Log.d("COMPATIBILIDAD", "fecha_caducidad: " + fecha_caducidad);
        Log.d("NFCTATI", " " + fecha_caducidad);
        String[] fechas = fecha_caducidad.split("-");
        int ano = Integer.parseInt(fechas[0]);
        int mes = Integer.parseInt(fechas[1]);
        int dia = Integer.parseInt(fechas[2]);
        Log.d("NFCTATI", " " + ano + " " + mes + " " + dia);

        Calendar calendar = new GregorianCalendar();
        Log.d("NFCTATI", " Chega dp fecha 2");

        //calendar.setTimeInMillis(date_in_milis);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int date = calendar.get(Calendar.DATE);
        Log.d("NFCTATI", " Chega dp fecha");

        if(year>ano) {
            return 1;
        } else if(year<ano) {
            return 0;
        } else if(ano == year) {
            if(month>mes) {
                return 1;
            } else if(month<mes) {
                return  0;
            } else if(month == mes) {
                if(date>dia) {
                    return 1;
                } else if(date<dia) {
                    return 0;
                } else if(date == dia) {
                    return 0;
                }
            }
        }
        return 0;
    }

    private boolean comprobacionFechas(String fecha) {
        String[] c = fecha.split("-");
        if (c.length == 3) {
            return true;
        } else {
            return false;
        }
    }

    private void comprobamos_compatibilidad(int id_med, int id_lote_a, String fecha_caducidad) throws ExeceptionPropia {
        Log.d("NFCTATI","Entra en funcion");
        if(comprobacionFechas(fecha_caducidad)){
            //if(fec_cad.equals(fecha_caducidad)) {
            if (caducado(fecha_caducidad) == 1) {
                Log.d("NFCTATI", "Entra en caducao");

                Toast.makeText(readNfcMed.this, "ERROR, IMPOSIBLE SUBMINISTRAR MEDICAMENTO, CADUCADO", Toast.LENGTH_SHORT).show();

            } else {
                Log.d("NFCTATI", "Entra en non caducao");

                if (id_med == id && lote == id_lote_a) {
                    Log.d("NFCMED", "MEDICAMENTO CORRECTO");
                    Log.d("NFCTATI", "Entra en correcto");

                    // Abriimos dialogo
                    medicacion_correcta();

                } else {
                    Log.d("NFCTATI", "Entra en incorrecto");

                    Toast.makeText(readNfcMed.this, "ERROR, IMPOSIBLE SUBMINISTRAR MEDICAMENTO, INCORRECTO", Toast.LENGTH_SHORT).show();

                }
            }
        /*} else {
            TextView t = new TextView(this);
            t.setText("ERROR, IMPOSIBLE SUBMINISTRAR MEDICAMENTO");
            Log.d("COMPATIBILIDAD", "Medi incorrecto");
            ll.addView(t);
        }*/
        } else {
            Toast.makeText(readNfcMed.this, "ERROR, FORMATO FECHA ERRONEO", Toast.LENGTH_SHORT).show();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_read_nfc_med, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void medicacion_correcta() {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.dialog_medadm);

        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("MEDICACION SUBMINISTRADA CORRECTAMENTE");



        //TextView contenido = (TextView) customDialog.findViewById(R.id.contenido);
        //contenido.setText("Mensaje con el contenido del dialog");

        ((Button) customDialog.findViewById(R.id.aceptar_medadm)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                // Actualizariamos BD de administracion/tomado
                Calendar calendar = new GregorianCalendar();
                //calendar.setTimeInMillis(date_in_milis);
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int date = calendar.get(Calendar.DATE);
                try {
                    adminMed.insertarAdmin(nhc, id, turno, lote, year+"-"+month+"-"+date);
                } catch (ExeceptionPropia execeptionPropia) {
                    execeptionPropia.printStackTrace();
                }
                customDialog.dismiss();
                Intent i = new Intent(readNfcMed.this, datos_ingreso_enf.class);
                i.putExtra("id_paciente", String.valueOf(nhc));
                i.putExtra("tipo_usuario", tipoUser);
                startActivity(i);
                Log.d("ADMIN", "CORRECTO");

            }
        });



        customDialog.show();
    }

    @Override
    public void onClick(View v) {
        if(v == but_nfc) {
            // Dialogo para informar que acerque etiqueta NFC
            FragmentManager fragmentManager = getSupportFragmentManager();
            dialogo = new DialogoAlerta();
            dialogo.show(fragmentManager, "tag");

            Log.d("NFC", "Creamos diaalogo");
            nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            detectedTag =getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
            pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                    new Intent(this, getClass()).
                            addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

            IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
            IntentFilter filter2     = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
            readTagFilters = new IntentFilter[]{tagDetected,filter2};
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, readTagFilters, null);
        }

    }
}
