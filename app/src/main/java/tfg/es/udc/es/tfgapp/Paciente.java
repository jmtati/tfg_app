package tfg.es.udc.es.tfgapp;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Juan Manuel on 14/03/2016.
 */
public class Paciente implements Parcelable {

    String nombre, apellidos, fec_nac, nacionalidad,estado, servicio;
    int nhc, edad, numero_cama;

    public int getNumero_cama() {
        return numero_cama;
    }

    public void setNumero_cama(int numero_cama) {
        this.numero_cama = numero_cama;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        String g = "M";
        if(this.genero.equals(Genero.FEM)) {
            g = "F";
        }
        dest.writeStringArray(new String[]{this.nombre,this.apellidos, String.valueOf(this.edad),
        g, this.fec_nac, this.nacionalidad, this.estado});
     }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Paciente> CREATOR = new Parcelable.Creator<Paciente>() {
        public Paciente createFromParcel(Parcel in) {
            return new Paciente(in);
        }

        public Paciente[] newArray(int size) {
            return new Paciente[size];
        }
    };

    public Paciente(Parcel in) {
        String[] data= new String[7];

        in.readStringArray(data);
        this.nombre= data[0];
        this.apellidos= data[1];
        this.edad = Integer.parseInt(data[2]);
        if(data[3].equals("M")) {
            this.genero = Genero.MASC;
        } else {
            this.genero = Genero.FEM;
        }
        this.fec_nac = data[4];
        this.nacionalidad = data[5];
        this.estado = data[6];

    }

    public enum Genero
    {
        MASC, FEM
    }
    Genero genero;


    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public Paciente(JSONObject objetoJSON) throws JSONException {
        nhc = objetoJSON.getInt("NHC");
        numero_cama = objetoJSON.getInt("num_cama");
        servicio = objetoJSON.getString("servicio");
        nombre = objetoJSON.getString("nombre");
        apellidos = objetoJSON.getString("apellidos");
        if(objetoJSON.getString("genero").equals("M")) {
            genero = Genero.MASC;
        } else {
            genero = Genero.FEM;
        }
        edad = objetoJSON.getInt("edad");
        fec_nac = objetoJSON.getString("fec_nac");
        nacionalidad = objetoJSON.getString("nacionalidad");
        estado = objetoJSON.getString("estado");
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }



    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getNhc() {
        return nhc;
    }

    public void setNhc(int id) {
        this.nhc = id;
    }

    public String getFec_nac() {
        return fec_nac;
    }

    public void setFec_nac(String fec_nac) {
        this.fec_nac = fec_nac;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }
}
