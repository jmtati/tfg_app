package tfg.es.udc.es.tfgapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.opengl.Visibility;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import android.graphics.Color;
import android.widget.Toast;


public class datos_ingreso extends ActionBarActivity implements AdapterView.OnItemClickListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    TextView tv_datospersonales, tv_name, tv_genero, tv_edad, tv_fecnac, tv_ingreso, tv_datosingreso, tv_hist,
    tv_nac, tv_nhc, tv_datosItitle, tv_datosPtitle, tv_datosP, tv_probasaportadastitle, tv_probas, tv_antecedentes,
            tv_antecedentesdatos, tv_datosubicacion;
    TextView  tv_cuidados, tv_complt, tv_tert, tv_diagt, tv_actt;
    List<Paciente> lista_pac;
    List<Ingreso> lista_ingre;
    ListView listView;
    int tipoUser;
    List<Medicamente_lista> items;
    List<Monitorizacion> mon;
    ArrayList<Medicacion> lista_medic;
    Button but_alerxias;
    CheckBox ch_alsi;
    EditText alerxias;
    LinearLayout ll, hm, pc;
    ImageButton btn_anadir;
    LinearLayout grafica_ll;
    CheckBox c_t, c_ta, c_pa;
    List<String> alerxias_list;
    String id = "";
    String[] mMonth = new String[] { "0h", "1h", "2h", "3h", "4h", "5h", "6h", "7h",
            "8h", "9h", "10h", "11h", "12h", "13h", "14h", "15h", "16h", "17h", "18h", "19h",
            "20h", "21h", "22h", "23h"
    };
    int id_notify = 0;
    Button but_compl, but_ter, but_diag, but_act;

    // Creamos bd
    IngresoSQLite ingresoSql;
    HistorialSQLite historialIngresoSql;
    InfoAltaSQLite infoaltaSql;
    HistorialMedicoSQLite historial_medicoSql;
    MonitorizacionSQLite monitorizacionSql;
    MedicamentosSQLite medicamentosSql;
    Ingreso pacienteIngresado = null;
    AlertasSQLite alertasSql;
    AdministracionMedSQLite adminMed;
    PlanosCuidadosSQLite planCuidados;
    List<Medicacion> items_administrar;
    ArrayList<Medicacion> lista_medic_final = new ArrayList();

    int estado_paciente = -1; // Inicializamos el estado 0 -> ingresado, 1 -> alta


    private NfcAdapter mNfcAdapter;
    Tag detectedTag;
    TextView txtType,txtSize,txtWrite,txtRead;
    NfcAdapter nfcAdapter;
    IntentFilter[] readTagFilters;
    PendingIntent pendingIntent;

    Dialog customDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_ingreso);
        try {
            lista_pac = cargar_BDPacientes();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        id = getIntent().getExtras().getString("id_paciente");
        Log.d("RECIVE", "ID: " + id);
        tipoUser = getIntent().getExtras().getInt("tipo_usuario");
        //String id = getIntent().getExtras().getString("id_paciente");
        Log.d("NFC DATOS", "ID: " + id);
        Log.d("USER:", " " + tipoUser);
       //Paciente pac = (Paciente) getIntent().getExtras().get("paciente");
        tv_datospersonales = (TextView) findViewById(R.id.datosPersonales_tv);
        tv_datosubicacion = (TextView) findViewById(R.id.tv_datosubicacion);
        tv_nhc = (TextView) findViewById(R.id.tv_nhc);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_genero = (TextView) findViewById(R.id.tv_genero);
        tv_edad = (TextView) findViewById(R.id.tv_edad);
        tv_fecnac = (TextView) findViewById(R.id.tv_fecnac);
        tv_nac = (TextView) findViewById(R.id.tv_nacionalidad);
        tv_ingreso = (TextView) findViewById(R.id.tv_ingreso);
        tv_datosItitle = (TextView) findViewById(R.id.tv_datostitle);
        tv_datosingreso = (TextView) findViewById(R.id.tv_datosingreso);
        tv_datosPtitle = (TextView) findViewById(R.id.tv_datospersonalestitle);
        tv_datosP = (TextView) findViewById(R.id.tv_datospersonales);
        tv_hist = (TextView) findViewById(R.id.tv_hist);
        tv_hist.setOnClickListener(this);
        tv_probasaportadastitle = (TextView) findViewById(R.id.tv_probasaportadastitle);
        tv_probas = (TextView) findViewById(R.id.tv_probas);
        ll = (LinearLayout) findViewById(R.id.datos_ingresoL);
        hm = (LinearLayout) findViewById(R.id.hist_l);
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        btn_anadir = (ImageButton) findViewById(R.id.btn_anadir);
        btn_anadir.setOnClickListener(this);
        c_t = (CheckBox) findViewById(R.id.ch_temp);
        c_ta = (CheckBox) findViewById(R.id.ch_ta);
        c_pa = (CheckBox) findViewById(R.id.ch_presion);
        c_t.setOnCheckedChangeListener(this);
        c_ta.setOnCheckedChangeListener(this);
        c_pa.setOnCheckedChangeListener(this);
        grafica_ll = (LinearLayout) findViewById(R.id.grafica_ll);
        pc = (LinearLayout) findViewById(R.id.planes_cuidado);
        tv_antecedentes = (TextView) findViewById(R.id.tv_antecedentestitle);
        tv_antecedentesdatos = (TextView) findViewById(R.id.tv_antecedentes);
        tv_complt  = (TextView) findViewById(R.id.tv_compltexto);
        tv_tert = (TextView) findViewById(R.id.tv_tertexto);
        tv_diagt  = (TextView) findViewById(R.id.tv_diagtexto);
        tv_actt  = (TextView) findViewById(R.id.tv_actexto);


        // Creamos bd
        ingresoSql = new IngresoSQLite(this);
        historialIngresoSql = new HistorialSQLite(this);
        infoaltaSql = new InfoAltaSQLite(this);
        historial_medicoSql = new HistorialMedicoSQLite(this);
        monitorizacionSql = new MonitorizacionSQLite(this);
        medicamentosSql = new MedicamentosSQLite(this);
        alertasSql = new AlertasSQLite(datos_ingreso.this);
        adminMed = new AdministracionMedSQLite(this);
        planCuidados = new PlanosCuidadosSQLite(this);


        // PRUEBAS
        //ingresoSql.eliminarBD();
        //infoaltaSql.eliminarBD();
        /*InformeAlta infoAlta = new InformeAlta(1, "ahora",
                "fasdasdas", "A", 0,0,0);
        infoaltaSql.insertarAlta(infoAlta);*/
                //historialIngresoSql.eliminarBD();
        /*historialIngresoSql.insertarHistorial(1, 3, "sisisis", "inicio" , "fin", "nada de nada", "ingreso");*/
        try {
            medicamentosSql.insertarMedicamento(1,1,"Paracetamol", 200, "oral", "2017-10-2");
            medicamentosSql.insertarMedicamento(2,1,"Paracetamol", 200, "oral", "2017-10-2");

            medicamentosSql.insertarMedicamento(3,1,"Paracetamol", 200, "oral", "2017-10-2");

            medicamentosSql.insertarMedicamento(4,1,"Paracetamol", 200, "oral", "2017-10-2");

            medicamentosSql.insertarMedicamento(5,1,"Paracetamol", 200, "oral", "2017-10-2");

            medicamentosSql.insertarMedicamento(6,1,"Paracetamol", 200, "oral", "2017-10-2");

            medicamentosSql.insertarMedicamento(7,1,"Paracetamol", 200, "oral", "2017-10-2");


        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
        }
        /*try {
            adminMed.insertarAdmin(1,1,1,1,"2015-5-1");
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
        }*/
        //medicamentosSql.insertarMedicamento(2,2,"Paracetamol", 20, "intra", "2017-10-5");
        //medicamentosSql.insertarMedicamento(3,3,"Paracetamol", 2, "extra", "2017-10-7");
        //adminMed.insertarAdmin(1, 1, 1, 1, "2017-5-4");
        //adminMed.insertarAdmin(1, 2, 1, 1, "2017-5-20");
        /*Vector vv = medicamentosSql.obtenerInfowName("Paracetamol");
        /*Log.d("VECTORES", "Size: " + vv.size());
        Log.d("VECTORES", "Valor: " + vv.get(0).toString());
        Log.d("VECTORES", "Valor: " + vv.get(1).toString());
        Log.d("VECTORES", "Valor: " + vv.get(2).toString());*/
        /*planCuidados.insertarCuidados(1,687,"Sino","Complicaciones posibles");
        Vector f = planCuidados.obtener(1);
        Log.d("CUIDADOS", "Size; "+ f.size());
        Calendar calendar = new GregorianCalendar();
        Calendar calendar2 = new GregorianCalendar();
        //calendar.setTimeInMillis(date_in_milis);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int date = calendar.get(Calendar.DATE);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String fecha =  year+"-"+month+"-"+date;
        calendar2.set(2014, 2, 12);
        Log.d("CALENDAR", fecha + ", " + calendar2.get(Calendar.YEAR));*/


        /************************************************************************/
        try {
            lista_ingre = cargar_ingresos();
            //lista_medic = lista_ingre.get(Integer.parseInt(id)-1).lista_medicacion;
            Log.d("INGRESOS", "Tamano: " + lista_ingre.size() + " " + lista_ingre.get(0).getId_i());
            Log.d("INGRESOS", "Pasamos por aqui");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();

        }
        pacienteIngresado = buscarIngreso(Integer.parseInt(id));
        /****************************************************************************/

        // DATOS PERSONALES - Para los 3 iguales
        estado_paciente = actualizar_dp(Integer.parseInt(id));
        if(pacienteIngresado != null) {
            // Comprobamos en el informe, pero podria a ver sido dado de alta por el medico, por lo tanto alerta.
            Vector v_alta = null;
            try {
                v_alta = alertasSql.obtenerAltas(Integer.parseInt(id));
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
                Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
            Vector v_esta = null;
            try {
                v_esta = ingresoSql.obtenerIngreso(pacienteIngresado.getId_i(), pacienteIngresado.getId());
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
                Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

            }
            // Quiere decir qe el ingreso fue insertado, por lo tanto ese id esta dado de alta.
            Log.d("INGRESOS", "Alta: " + v_alta.size() + ", " + v_esta.size());
            if (v_esta.size() > 0) {
                estado_paciente = 1;
            }

            Log.d("ESTADO", "Estado: " + estado_paciente);

        }


        /************************************************************************/

        // GRAFICA DE MONITORIZACION DE CONSTANTES - Para los 3 iguales
        if(estado_paciente == 0) { // Está ingresado
            mon = new ArrayList<Monitorizacion>();
            try {
                mon = cargar_monitorizacion(Integer.parseInt(id));
                //monitorizacionSql.eliminarBD();
                //Log.d("MONITORIZACION", "CARGAMOS " + mon.get(Integer.parseInt(id) - 1).getDatos().size());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            drawChart(mon, 0, Integer.parseInt(id));
        } else {
            // Si no esta ingresado ocultamos la grafica de monitorizacion
            LinearLayout chartContainer = (LinearLayout) findViewById(R.id.grafica_ll);
            LinearLayout chartContainer2 = (LinearLayout) findViewById(R.id.chart_container);
            //chartContainer.setVisibility(View.INVISIBLE);
            chartContainer.removeAllViews();
            //chartContainer2.setVisibility(View.INVISIBLE);
            chartContainer2.removeAllViews();

        }

        /************************************************************************/

        // ENFERMERO-MEDICO
        // - INGRESO: Para ambos
        Log.d("INGRESOS", "Pasamos por aqui 2");

        if(estado_paciente == 0) {
            //Mostraiamos datos del ingreso
                tv_datosItitle.setText("DATOS DO INGRESO");
                String datos_ingreso = "Fecha: " + pacienteIngresado.getData() + "\n" +
                        "Procedencia: " + pacienteIngresado.getPro() + "\n" +
                        "Diagnostico: " + pacienteIngresado.getDiag() + "\n" +
                        "Contastantes: " + "\n" +
                        "- TA: " + pacienteIngresado.getTa() + "\n" +
                        "- FC: " + pacienteIngresado.getFc() + "\n" +
                        "- T: " + pacienteIngresado.getT() + "\n" +
                        "- FR: " + pacienteIngresado.getFr() + "\n" +
                        "- PESO: " + pacienteIngresado.getPeso() + "\n" +
                        "- TALLA: " + pacienteIngresado.getTalla() + "\n";
                tv_datosingreso.setText(datos_ingreso);
                tv_datosPtitle.setText("DATOS PERSONALES");
                String datos_personales = "Tlf contacto: " + pacienteIngresado.getTlfcontacto() +
                        "\n" + "Centro de saude: " + pacienteIngresado.getCentro() + "\n";
                tv_datosP.setText(datos_personales);
                tv_probasaportadastitle.setText("PROBAS APORTADAS");
                String texto = "";
                for (int i = 0; i < pacienteIngresado.probasaportadas_lista.size(); i++) {
                    texto = texto + pacienteIngresado.getProbasaportadas_lista().get(i);
                }
                tv_probas.setText(texto);
                tv_antecedentes.setText("ALERXIAS");
                String text = "";
                for (int i = 0; i < pacienteIngresado.alerxias.size(); i++) {
                    text = text + pacienteIngresado.getAlerxias().get(i);
                }
                tv_antecedentesdatos.setText(text);




        } else {
            //ll.setVisibility(View.INVISIBLE);
            ll.removeAllViews();
        }

        /************************************************************************/
        // Historial clinico

        /************************************************************************/

        /************************************************************************/


        // MEDICAMENTOS
        if(estado_paciente == 0) {
        // Esto quiere decir que está ingresado
            obtenerMedicacion();
            Log.d("TATIANO","Size: " + pacienteIngresado.lista_medicacion.size());
            mostrarListaMed(tipoUser, lista_medic_final);
        } else { // Obtendriamos todas las altas de ese paciente
            // Vector med = infoaltaSql.obtenerMedicacion(Integer.parseInt(id));
            ArrayList<Medicacion> l_medic_actual = obtenerMedicacionAlta();
            if (l_medic_actual != null) {
                mostrarListaMed(tipoUser, l_medic_actual);
                Log.d("FECHAALTA", "FINAL: " + l_medic_actual.size());
            }
        }








    }

    public ArrayList<Medicacion> obtenerMedicacionAlta() {

        ArrayList<Medicacion> l_medic_actual;
        //mostrarListaMed(tipoUser, med);
        // Mostrariamos med
        Vector v2 = new Vector();
        try {
            v2 = historialIngresoSql.obtenerMedicacionAlta(pacienteIngresado.getId());
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
            Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }
        Log.d("MED-ACTUAL", "TAMANO MEDICACION ACTUAL:" + v2.size());

        if (v2.size() > 0) {
            l_medic_actual = new ArrayList<Medicacion>();
            for (int it = 0; it < v2.size(); it++) {
                // id_medicamento, pautas, fecha_inicio, fecha_fin, observacions, tipo
                String[] s = v2.get(it).toString().split(",");
                Log.d("MED-ACTUAL", "Med: " + s[0] + "," + s[3]);

                String[] fecha_valor = s[3].toString().split("-");
                Log.d("FECHAALTA", "Result: " + comprobacion_fecha(fecha_valor));
                if (comprobacion_fecha(fecha_valor) == 1) {

                    Vector v3 = null;
                    try {
                        v3 = medicamentosSql.obtenerInfo(Integer.parseInt(s[0]));
                    } catch (ExeceptionPropia execeptionPropia) {
                        execeptionPropia.printStackTrace();
                        Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

                    }
                    if (v3.size() > 0) {
                        // id, lote, nombre, dosis, vias, fecha_cad
                        String[] s2 = v3.get(0).toString().split(",");

                        Medicacion medPre = new Medicacion(Integer.parseInt(s[0]), s2[2], Double.parseDouble(s2[3]),
                                Integer.parseInt(s2[1]), s[1], s2[4], s[2], s[3], s[4], s[5], s2[5], 1);
                        l_medic_actual.add(medPre);

                    }
                }
            }
            return l_medic_actual;
        }
        return null;
    }

    public void obtenerMedicacion() {
        Vector v1 = null;
        try {
            v1 = historialIngresoSql.obtenerHistorial(pacienteIngresado.getId_i(), pacienteIngresado.getId(), pacienteIngresado.getData());
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
            Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }
        Log.d("NUEVAMED", "V1: " + v1.size());
        if(v1.size()>0) {
            // Cuando la medicacion ya esta insertada
            for(int i = 0;i<v1.size();i++) {
                // id_medicamento, pautas, fecha_inicio, fecha_fin, observacions, dosis, vias
                String[] val = v1.get(i).toString().split(",");
                // id, nombre, lote, fecha_cad
                Vector vec = null;
                try {
                    vec = medicamentosSql.obtener(Integer.parseInt(val[0]));
                } catch (ExeceptionPropia execeptionPropia) {
                    execeptionPropia.printStackTrace();
                    Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

                }
                String[] valores = vec.get(0).toString().split(",");

                String nombre = valores[1].toString();
                Double dosis = Double.parseDouble(val[5].toString());
                String pautas = val[1].toString();
                String fecha_i = val[2].toString();
                String fecha_f = val[3].toString();
                String ob = val[4].toString();
                String viass = val[6].toString();

                int idMed = Integer.parseInt(valores[0].toString());
                int lote = Integer.parseInt(valores[2].toString());
                String fecCad = valores[3].toString();
                Medicacion med = new Medicacion(idMed, nombre, dosis,
                        lote, pautas, viass, fecha_i,
                        fecha_f, ob, "ingreso",
                        fecCad, 1);
                lista_medic_final.add(med);

            }
            Log.d("NUEVAMED", "Med one: " + lista_medic_final.size());
        } else {
            // Insertamos solo la 1 vez
            lista_medic_final = listMedCompleta();
            insertarMedicacion(lista_medic_final);
            Log.d("NUEVAMED", "Med two: " + lista_medic_final.size());
        }
    }

    private int comprobacion_fecha(String[] fecha_fin) {
        int anho = Integer.parseInt(fecha_fin[0].toString());
        int mes = Integer.parseInt(fecha_fin[1].toString());
        int dia = Integer.parseInt(fecha_fin[2].toString());
        Log.d("FECHAALTA", "1: " + anho +","+mes+","+dia);

        Calendar c = new GregorianCalendar();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        month+=1;
        int date = c.get(Calendar.DATE);
        Log.d("FECHAALTA", "2: " + year +","+month+","+date);

        if(anho > year) {
            return 1;
        } else if (anho<year){
            return 0;
        } else if( anho == year) {
            if(mes > month) {
                return 1;
            } else if(mes<month) {
                return 0;
            } else if(month == mes) {
                if(dia > date ) {
                    return 1;
                } else if(dia<date) {
                    return 0;
                } else {
                    return 0;
                }
            }
        }
        return -1;
    }

    public ArrayList<Medicacion> listMedCompleta() {
        lista_medic = pacienteIngresado.lista_medicacion;
        ArrayList<Medicacion> lista_medic_final = new ArrayList<Medicacion>();
        for(int i = 0;i<lista_medic.size();i++) {
            Vector v = medicamentosSql.obtenerInfowName(lista_medic.get(i).getNombre());
            Log.d("NUEVAMED", "v2: " + v.size());
            int j = 1;
            if(v.size()>0) {
                // id, lote, dosis, vias, fecha_cad, usado
                String[] valores = v.get(0).toString().split(",");
                // Hasta que encontremos uno dispoible
                while (j < v.size() && Integer.parseInt(valores[5].toString()) != 0) {

                    // id, lote, dosis, vias, fecha_cad, usado
                    valores = v.get(j).toString().split(",");
                    j++;
                }
                int idMed = Integer.parseInt(valores[0].toString());
                int lote = Integer.parseInt(valores[1].toString());
                String fecCad = valores[4].toString();
                // Ese id del medicamento esta usado.
                try {
                    medicamentosSql.insertarUsado(idMed, lote, lista_medic.get(i).getNombre(), lista_medic.get(i).getDosis(),
                            lista_medic.get(i).getVias().toString(),fecCad );
                } catch (ExeceptionPropia execeptionPropia) {
                    execeptionPropia.printStackTrace();
                    Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

                }

                Medicacion med = new Medicacion(idMed, lista_medic.get(i).getNombre(), lista_medic.get(i).getDosis(),
                        lote, lista_medic.get(i).getPautas(), lista_medic.get(i).getVias().toString(), lista_medic.get(i).getFecha_ini(),
                        lista_medic.get(i).getFecha_final(), lista_medic.get(i).getObservaciones(), "ingreso",
                        fecCad, 1);
                lista_medic_final.add(med);
            } else {
                Toast.makeText(this, "El medicamento con nombre: " + lista_medic.get(i).getNombre() + ", no se encuentra" +
                        "registado en nuestra base de datos.", Toast.LENGTH_SHORT).show();
            }
        }
        return lista_medic_final;
    }

    public int comprobar_alertas() {
        Vector v = null; // Devuelve solo las aletas de nuevo medicamento
        try {
            v = alertasSql.obtener(pacienteIngresado.getId());
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
            Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }
        Log.d("ALERTAS 2", "Hay alerta: " + v.size());
        if(v.size()>0) {
            for(int i = 0;i<v.size();i++) {
                String[] sep = v.get(i).toString().split(",");
                Log.d("ALERTAS 2", sep[0] + " " + sep[1]);
                if(sep[1].toString().equals("alta")) {
                    Notify("Alta paciente: ", "NHC: " + sep[0]);

                } else {
                    Notify("Nueva medicacion: ", "NHC: " + sep[0] + " Nombre:" + sep[1]);
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    private int insertarMedicacion(List<Medicacion> med) {
        Vector r = null;
        try {
            r = historialIngresoSql.obtenerHistorial(pacienteIngresado.getId_i(), pacienteIngresado.getId(), "todos");
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
            Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }
        if(r.size() < med.size()) {
            for (int i = 0; i < med.size(); i++) {

                try {
                    historialIngresoSql.insertarHistorial(pacienteIngresado.getId_i(), pacienteIngresado.getId(), pacienteIngresado.getData()
                            , med.get(i).getId(), med.get(i).getPautas(), med.get(i).getFecha_ini(), med.get(i).getFecha_final(), med.get(i).getObservaciones(),
                            med.get(i).getTipo(), med.get(i).getDosis(), med.get(i).getVias());
                } catch (ExeceptionPropia execeptionPropia) {
                    execeptionPropia.printStackTrace();
                    Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

                }
            }

        }
        return 0;
    }

    private void Notify(String notificationTitle, String notificationMessage){
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        @SuppressWarnings("deprecation")

        Notification notification = new Notification(R.drawable.abc_ab_share_pack_holo_dark,"New Message", System.currentTimeMillis());
        Intent notificationIntent = new Intent(this,MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        notification.setLatestEventInfo(datos_ingreso.this, notificationTitle,notificationMessage, pendingIntent);
        notificationManager.notify(id_notify, notification);
        id_notify += 1;
    }

    private int calcularTurno() {
        Calendar calendario = Calendar.getInstance();
        //Calendar calendario = new GregorianCalendar();
        int hora, minutos, segundos;
        hora =calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);
        Log.d("TURNO", "Horas: " + hora + " " + minutos);
        if(hora>4 && hora<12) {
            return 1;
        } else {
            if(hora>12 && hora<20) {
                return 2;
            } else {
                return 3;
            }
        }
    }



    public Ingreso buscarIngreso(int nhc) {
        for(int i = 0;i<lista_ingre.size();i++) {
            if(lista_ingre.get(i).getId() == nhc) {
                return lista_ingre.get(i);
            }
        }
        return null;

    }

    private void drawChart(List<Monitorizacion> mon, int lin, int nhc){
        // Como guardar datos?
        int[] x = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24 };
        int[] temperatura = new int[24];
        int[] pulso = new int[24];
        int[] ta = new int[24];
        for(int i = 0;i<mon.size();i++) {
                temperatura[mon.get(i).getTurno()] = mon.get(i).getT();
                pulso[mon.get(i).getTurno()] = mon.get(i).getPa();
                ta[mon.get(i).getTurno()] = mon.get(i).getTa();
        }

        Log.d("MONITORIZACION", "jeje2: " + temperatura.length + ", " + mon.size());

        // Creating an  XYSeries for Income
        XYSeries temperaturaSeries = new XYSeries("Temperatura");
        // Creating an  XYSeries for Expense
        XYSeries pulsoSeries = new XYSeries("Pulso");
        XYSeries taSeries = new XYSeries("T.A.");

        // Adding data to Income and Expense Series
        for(int i=0;i<x.length;i++){
            temperaturaSeries.add(x[i], temperatura[i]);
            pulsoSeries.add(x[i],pulso[i]);
            taSeries.add(x[i], ta[i]);
        }
        XYMultipleSeriesDataset xyMultipleSeriesDataset = new XYMultipleSeriesDataset();
        // Creating a dataset to hold each series
        if(lin == 0) {

            // Adding Income Series to the dataset
            xyMultipleSeriesDataset.addSeries(temperaturaSeries);
            // Adding Expense Series to dataset
            xyMultipleSeriesDataset.addSeries(pulsoSeries);
            xyMultipleSeriesDataset.addSeries(taSeries);
        } else {
            if(lin == 1) {
                // Adding Expense Series to dataset
                xyMultipleSeriesDataset.addSeries(pulsoSeries);
                xyMultipleSeriesDataset.addSeries(taSeries);
            } else {
                if(lin == 2) {
                    // Adding Expense Series to dataset
                    xyMultipleSeriesDataset.addSeries(pulsoSeries);
                    xyMultipleSeriesDataset.addSeries(temperaturaSeries);
                } else {
                    // Adding Expense Series to dataset
                    xyMultipleSeriesDataset.addSeries(taSeries);
                    xyMultipleSeriesDataset.addSeries(temperaturaSeries);
                }
            }
        }
        // Creating XYSeriesRenderer to customize incomeSeries
        XYSeriesRenderer temperaturaRenderer = new XYSeriesRenderer();
        temperaturaRenderer.setColor(Color.RED);
        temperaturaRenderer.setPointStyle(PointStyle.CIRCLE);
        temperaturaRenderer.setFillPoints(true);
        temperaturaRenderer.setLineWidth(2);
        temperaturaRenderer.setDisplayChartValues(true);

        // Creating XYSeriesRenderer to customize expenseSeries
        XYSeriesRenderer pulsoRenderer = new XYSeriesRenderer();
        pulsoRenderer.setColor(Color.BLUE);
        pulsoRenderer.setPointStyle(PointStyle.CIRCLE);
        pulsoRenderer.setFillPoints(true);
        pulsoRenderer.setLineWidth(2);
        pulsoRenderer.setDisplayChartValues(true);

        XYSeriesRenderer taRenderer = new XYSeriesRenderer();
        taRenderer.setColor(Color.GREEN);
        taRenderer.setPointStyle(PointStyle.CIRCLE);
        taRenderer.setFillPoints(true);
        taRenderer.setLineWidth(2);
        taRenderer.setDisplayChartValues(true);

        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setXLabels(0);
        multiRenderer.setChartTitle("MONITORIZACION");
        multiRenderer.setXTitle("HORAS");
        multiRenderer.setYTitle("Temp-Pulso-TA");
        multiRenderer.setZoomButtonsVisible(true);
        for(int i=0;i<x.length;i++){
            multiRenderer.addXTextLabel(i+1, mMonth[i]);
        }

        // Adding incomeRenderer and expenseRenderer to multipleRenderer
        // Note: The order of adding dataseries to dataset and renderers to multipleRenderer
        // should be same
        if(lin == 0) {
            multiRenderer.addSeriesRenderer(temperaturaRenderer);
            multiRenderer.addSeriesRenderer(taRenderer);
            multiRenderer.addSeriesRenderer(pulsoRenderer);
        } else {
            if(lin == 1) {
                multiRenderer.addSeriesRenderer(taRenderer);
                multiRenderer.addSeriesRenderer(pulsoRenderer);
            } else {
                if(lin == 2) {
                    multiRenderer.addSeriesRenderer(pulsoRenderer);
                    multiRenderer.addSeriesRenderer(temperaturaRenderer);
                } else {
                    multiRenderer.addSeriesRenderer(taRenderer);
                    multiRenderer.addSeriesRenderer(temperaturaRenderer);
                }
            }
        }


        // Getting a reference to LinearLayout of the MainActivity Layout
        LinearLayout chartContainer = (LinearLayout) findViewById(R.id.grafica_ll);

        // Creating a Line Chart
        View chart = ChartFactory.getLineChartView(getBaseContext(), xyMultipleSeriesDataset, multiRenderer);


        // Adding the Line Chart to the LinearLayout
        Log.d("GRAFICA", "Pintamos la grafica");
        chartContainer.removeAllViews();
        chartContainer.addView(chart, 375, 375);
    }

    private void mostrarListaMed(int tipo, ArrayList<Medicacion> lista_med) {

        Log.d("LISTAMEDIC", "Numero TATI 2 " + lista_med.size());
        //listView.setAdapter(new ItemAdapterEM(this, lista_med));
        MedAuxAdapter adapter;
// Inicializamos el adapter.
        adapter = new MedAuxAdapter(this, lista_med);
// Asignamos el Adapter al ListView, en este punto hacemos que el
// ListView muestre los datos que queremos.
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_datos_ingreso, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int v = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (v == R.id.action_settings) {
            return true;
        }
        if(v == R.id.alta_paciente) {
            if(estado_paciente == 0) {
                if (id != "") {
                    alta_paciente(Integer.parseInt(id));
                }
            }
        }
        if(v == R.id.nuevo_medicamento) {
            if(estado_paciente == 0) {
                if (id != "") {
                    preescripcion_nuevomedicamento(Integer.parseInt(id));
                }
            }
        }
        if(v == R.id.buscar_lote) {
                buscar_pacientes_lote();

        }

        if(v == R.id.cerrar_sesion) {
            cerrar_sesion();

        }

        return super.onOptionsItemSelected(item);
    }

    private void cerrar_sesion() {
        //Intent intent = new Intent(datos_ingreso.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Intent intent = new Intent(datos_ingreso.this, MainActivity.class);
        finish();
        startActivity(intent);
    }

    private void buscar_pacientes_lote() {
        /*AlertDialog.Builder dialog = new AlertDialog.Builder(datos_ingreso.this);
        dialog.setTitle("Introduzca datos");
        Context context = datos_ingreso.this;
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        dialog.setTitle("BUSCAR LOTE");

        final EditText r = new EditText(context);
        r.setHint("Nombre medicamento");
        layout.addView(r);
        final EditText h = new EditText(context);
        h.setHint("Nº de lote");
        layout.addView(h);


        dialog.setView(layout);

        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                //Log.d("DIALOG", "T:" + temperatura.getText() + ta.getText() + p.getText());
                String lote_l = h.getText().toString();
                String nombre_medicamento =  r.getText().toString();
                int lote_buscar = Integer.parseInt(lote_l);
                Intent intent = new Intent(datos_ingreso.this, Lotesconpacientes.class);
                intent.putExtra("nombre_med",nombre_medicamento);
                intent.putExtra("lote_buscar", lote_buscar);
                startActivity(intent);

            }
        });

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        dialog.show();*/
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.dialog_lote);

        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("BUSCAR PACIENTES CON LOTE SUBMINISTRADO");

        final EditText nombre_med = (EditText) customDialog.findViewById(R.id.nombre_medlote);

        final EditText num_lote = (EditText) customDialog.findViewById(R.id.num_lote);


        //TextView contenido = (TextView) customDialog.findViewById(R.id.contenido);
        //contenido.setText("Mensaje con el contenido del dialog");

        ((Button) customDialog.findViewById(R.id.aceptar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                //Log.d("DIALOG", "T:" + temperatura.getText() + ta.getText() + p.getText());
                String lote_l = num_lote.getText().toString();
                String nombre_medicamento =  nombre_med.getText().toString();
                Log.d("LOTEMAS", ":'" + lote_l.length()+"'");
                if(lote_l.length()>0 && nombre_medicamento.length()>0) {
                    int lote_buscar = Integer.parseInt(lote_l);
                    Intent intent = new Intent(datos_ingreso.this, Lotesconpacientes.class);
                    intent.putExtra("nombre_med", nombre_medicamento);
                    intent.putExtra("lote_buscar", lote_buscar);
                    startActivity(intent);
                    customDialog.dismiss();
                } else {
                    Toast.makeText(datos_ingreso.this, "ERROR EN LOS DATOS", Toast.LENGTH_SHORT).show();
                }

            }
        });

        ((Button) customDialog.findViewById(R.id.cancelar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Toast.makeText(datos_ingreso.this, "CANCELAR", Toast.LENGTH_SHORT).show();

            }
        });

        customDialog.show();



    }

    private void preescripcion_nuevomedicamento(final int nhc) {
        /*AlertDialog.Builder dialog = new AlertDialog.Builder(datos_ingreso.this);
        dialog.setTitle("Introduzca datos");
        Context context = datos_ingreso.this;
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        dialog.setTitle("NUEVA MEDICACION");

        final EditText p = new EditText(context);
        p.setHint("Nombre");
        layout.addView(p);

        final EditText d = new EditText(context);
        d.setHint("Dosis");
        layout.addView(d);

        final EditText tra = new EditText(context);
        tra.setHint("Pautas");
        layout.addView(tra);

        final EditText vias = new EditText(context);
        vias.setHint("Vias");
        layout.addView(vias);

        final EditText temperatura = new EditText(context);
        temperatura.setHint("Fecha inicio");
        layout.addView(temperatura);

        final EditText ta = new EditText(context);
        ta.setHint("Fecha fin");
        layout.addView(ta);

        final EditText ob = new EditText(context);
        ob.setHint("Observaciones");
        layout.addView(ob);

        final EditText tipo = new EditText(context);
        tipo.setHint("Alta/Ingreso");
        layout.addView(tipo);

        dialog.setView(layout);

        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                //Datos basicos del medicamento
                String nombre = p.getText().toString();
                String dos = d.getText().toString();
                String pautas = tra.getText().toString();
                String vi = vias.getText().toString();
                String fecha_inicio = temperatura.getText().toString();
                String fecha_fin = ta.getText().toString();
                String obser = ob.getText().toString();
                String tip = tipo.getText().toString();
                pres_medicacion(nhc, nombre, dos, pautas, vi, fecha_inicio,fecha_fin, obser,tip);


            }
        });

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        dialog.show();*/
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.dialog_prees);

        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("PRESCRIPCION DEL MEDICAMENTO");

        final EditText nombre_med = (EditText) customDialog.findViewById(R.id.nombre_medpres);

        final EditText dosis_e = (EditText) customDialog.findViewById(R.id.dosis_p);
        final EditText pautas_e = (EditText) customDialog.findViewById(R.id.pautas_p);

        final EditText vias_e = (EditText) customDialog.findViewById(R.id.vias_p);

        final EditText fechaI_e = (EditText) customDialog.findViewById(R.id.fecha_inicio_p);

        final EditText fechaF_e = (EditText) customDialog.findViewById(R.id.fecha_fin_p);

        final EditText obs_e = (EditText) customDialog.findViewById(R.id.obs_p);

        final EditText alin_e = (EditText) customDialog.findViewById(R.id.altaingreso_p);



        //TextView contenido = (TextView) customDialog.findViewById(R.id.contenido);
        //contenido.setText("Mensaje con el contenido del dialog");

        ((Button) customDialog.findViewById(R.id.aceptar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                //Datos basicos del medicamento
                String nombre = nombre_med.getText().toString();
                String dos = dosis_e.getText().toString();
                String pautas = pautas_e.getText().toString();
                String vi = vias_e.getText().toString();
                String fecha_inicio = fechaI_e.getText().toString();
                String fecha_fin = fechaF_e.getText().toString();
                String obser = obs_e.getText().toString();
                String tip = alin_e.getText().toString().toLowerCase();

                if(nombre.length()>0 && dos.length()>0 && pautas.length()>0 && vi.length()>0 && fecha_inicio.length()>0
                        && fecha_fin.length()>0 && obser.length()>0 && tip.length()>0 ) {
                    pres_medicacion(nhc, nombre, dos, pautas, vi, fecha_inicio, fecha_fin, obser, tip);
                    customDialog.dismiss();
                } else {
                    Toast.makeText(datos_ingreso.this, "ERROR EN LOS DATOS", Toast.LENGTH_SHORT).show();
                }

            }
        });

        ((Button) customDialog.findViewById(R.id.cancelar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Toast.makeText(datos_ingreso.this, "CANCELAR", Toast.LENGTH_SHORT).show();

            }
        });

        customDialog.show();

    }

    public int pres_medicacion(int nhc,String nombre, String dos, String pautas, String vi, String fecha_inicio,
                                String fecha_fin, String obser, String tip) {
        Vector v = medicamentosSql.obtenerInfowName(nombre);
        int j = 1;
        // id, lote, dosis, vias, fecha_cad, usado
        String[] valores = v.get(0).toString().split(",");
        Log.d("VALORES", valores[5].toString() + " " + v.size());
        while(j<v.size() && Integer.parseInt(valores[5].toString())!=0) {
            Log.d("VALORES", valores[5].toString() + " " + valores[0]);
            // id, lote, dosis, vias, fecha_cad, usado
            valores = v.get(j).toString().split(",");
            j++;
        }
        if(j>=v.size()) {
            Toast.makeText(this, "NO HAY MEDICAMENTOS LIBRES", Toast.LENGTH_SHORT).show();
            return 0;
        } else {

            int idMed = Integer.parseInt(valores[0].toString());
            int lote = Integer.parseInt(valores[1].toString());
            String fecCad = valores[4].toString();

            // Ese id del medicamento esta usado.
            try {
                medicamentosSql.insertarUsado(idMed, lote, nombre, Double.parseDouble(dos),
                        vi, fecCad);
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
                Log.d("DATOSPRESS", "id " + idMed);
                Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

            }

            Medicacion med = new Medicacion(idMed, nombre, Double.parseDouble(dos),
                    lote, pautas, vi, fecha_inicio,
                    fecha_fin, obser, tip,
                    fecCad, 1);

            lista_medic_final.add(med);
            /*if(estado_paciente == 0) {
                mostrarListaMed(tipoUser, lista_medic_final);
            }*/

            // Insertariamos nueva medicacion para el nhc recibido y alerta
            // Creamos bd
            try {
                historialIngresoSql.insertarHistorial(pacienteIngresado.getId_i(), nhc, pacienteIngresado.getData(), idMed, pautas,
                        fecha_inicio, fecha_fin, obser, tip, Double.parseDouble(dos), vi);
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
                Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

            }
            try {
                alertasSql.insertarAlerta(nhc, nombre, "nuevo med");
            } catch (ExeceptionPropia execeptionPropia) {
                execeptionPropia.printStackTrace();
                Toast.makeText(this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

            }
            return 1;
        }

    }

    private void alta_paciente(final int nhc) {
        /*AlertDialog.Builder dialog = new AlertDialog.Builder(datos_ingreso.this);
        dialog.setTitle("Introduzca datos");
        Context context = datos_ingreso.this;
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        dialog.setTitle("Exploracion fisica");

        final EditText p = new EditText(context);
        p.setHint("Presion arterial");
        layout.addView(p);

        final EditText temperatura = new EditText(context);
        temperatura.setHint("Temperatura");
        layout.addView(temperatura);

        final EditText ta = new EditText(context);
        ta.setHint("Pulso");
        layout.addView(ta);

        final EditText especualidad = new EditText(context);
        especualidad.setHint("Especialidad");
        layout.addView(especualidad);

        // Preescripcion medica.
        /*final EditText tra = new EditText(context);
        tra.setHint("Tratamientos");
        layout.addView(tra);*/

        /*final EditText rec = new EditText(context);
        rec.setHint("Recomendaciones");
        layout.addView(rec);


        dialog.setView(layout);

        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                //Log.d("DIALOG", "T:" + temperatura.getText() + ta.getText() + p.getText());
                //int t = Integer.parseInt(temperatura.getText().toString());
                //int tai = Integer.parseInt(ta.getText().toString());
                //int pi = Integer.parseInt(p.getText().toString());

                // Guardar en sql, ingreso, historial y alta.
                Log.d("PUNTUACIONES", "CREAMOS HISTORIAL MEDICO");
                //ingresoSql.eliminarBD();
               // historialSql.eliminarBD();
               // infoaltaSql.eliminarBD();

                // Insertarmos el ingreso, historial y el alta
                try {
                    ingresoSql.insertarIngreso(pacienteIngresado);
                } catch (ExeceptionPropia execeptionPropia) {
                    execeptionPropia.printStackTrace();
                }
                //historialSql.insertarHistorial(1, "lalala", 50);
                String presion = p.getText().toString();
                String temp = temperatura.getText().toString();
                String tensionart = ta.getText().toString();

                alt_pac(nhc,presion,temp,tensionart,rec.getText().toString(), especualidad.getText().toString());

                // Mostrariamos todos los datos del alta. El informe
                AlertDialog.Builder dialog2 = new AlertDialog.Builder(datos_ingreso.this);
                dialog2.setTitle("ALTA PACIENTE");
                Context context = datos_ingreso.this;
                LinearLayout layout = new LinearLayout(context);
                final TextView texto = new TextView(context);
                layout.addView(texto);
                final String datos = "Presion: " +presion +
                        "\nTemperatura: " + temp +
                        "\nPulso: " + tensionart +
                        "\nEspecialidad: " + especualidad.getText().toString() +
                        "\nRecomendaciones: " + rec.getText().toString();
                texto.setText(datos);
                dialog2.setView(layout);
                dialog2.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Intent i_d = new Intent(datos_ingreso.this, datos_ingreso.class);
                                i_d.putExtra("id_paciente", String.valueOf(id));
                                i_d.putExtra("tipo_usuario", tipoUser);
                                startActivity(i_d);
                            }
                        });

                    dialog2.show();


                }
            });

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        dialog.show();*/

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.dialog);

        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("ALTA PACIENTE");

        final EditText pulso = (EditText) customDialog.findViewById(R.id.pulso);

        final EditText temp = (EditText) customDialog.findViewById(R.id.temperatura);

        final EditText tension = (EditText) customDialog.findViewById(R.id.tensionA);

        final EditText espec = (EditText) customDialog.findViewById(R.id.especialidad);

        final EditText recom = (EditText) customDialog.findViewById(R.id.recomendaciones);

        //TextView contenido = (TextView) customDialog.findViewById(R.id.contenido);
        //contenido.setText("Mensaje con el contenido del dialog");

        ((Button) customDialog.findViewById(R.id.aceptar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                // Insertarmos el ingreso, historial y el alta
                try {
                    ingresoSql.insertarIngreso(pacienteIngresado);
                } catch (ExeceptionPropia execeptionPropia) {
                    execeptionPropia.printStackTrace();
                    Toast.makeText(datos_ingreso.this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

                }
                //historialSql.insertarHistorial(1, "lalala", 50);
                String pulso_s = pulso.getText().toString();
                String temp_s = temp.getText().toString();
                String ten_s = tension.getText().toString();
                String espec_s = espec.getText().toString();
                String recom_s = recom.getText().toString();


                if(pulso_s.length()>0 && temp_s.length()>0 && ten_s.length()>0
                        && espec_s.length()>0 && recom_s.length()>0) {
                    alt_pac(nhc, pulso_s, temp_s, ten_s, recom_s, espec_s);
                    customDialog.dismiss();
                    Toast.makeText(datos_ingreso.this, "PACIENTE DADO DE ALTA", Toast.LENGTH_SHORT).show();
                    Intent i_d = new Intent(datos_ingreso.this, datos_ingreso.class);
                    i_d.putExtra("id_paciente", String.valueOf(id));
                    i_d.putExtra("tipo_usuario", tipoUser);
                    startActivity(i_d);
                    finish();
                } else {
                    Toast.makeText(datos_ingreso.this, "ERROR EN LOS DATOS", Toast.LENGTH_SHORT).show();
                }

            }
        });

        ((Button) customDialog.findViewById(R.id.cancelar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                Toast.makeText(datos_ingreso.this, "CANCELAR", Toast.LENGTH_SHORT).show();

            }
        });

        customDialog.show();

    }

    public void alt_pac(int nhc, String presion, String temp, String tensionart, String rec, String especialidad) {
        Calendar cal = new GregorianCalendar();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date = cal.get(Calendar.DATE);
        // Creamos e insertamos el alta
        Log.d("VALORES", presion + "," + temp + "," + tensionart);
        InformeAlta infoAlta = new InformeAlta(pacienteIngresado.getId(), year+"-"+month+"-"+date,
                pacienteIngresado.getData(), rec,Integer.parseInt(presion), Integer.parseInt(temp), Integer.parseInt(tensionart));
        int idAlta = 0;
        try {
            idAlta = infoaltaSql.insertarAlta(infoAlta);
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
            Toast.makeText(datos_ingreso.this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }

        try {
            historial_medicoSql.insertarHistorialMedico(pacienteIngresado.getId(), especialidad, pacienteIngresado.getData(),
                    pacienteIngresado.getId_i(), pacienteIngresado.getId_i(), idAlta);
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
            Toast.makeText(datos_ingreso.this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }
        try {
            monitorizacionSql.eliminarFila(pacienteIngresado.getId());
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
            Toast.makeText(datos_ingreso.this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }
        try {
            alertasSql.insertarAlerta(pacienteIngresado.getId(), "alta", "alta");
        } catch (ExeceptionPropia e) {
            e.printStackTrace();
            Toast.makeText(datos_ingreso.this, "Error en los datos: id = " + e.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }

        try {
            planCuidados.eliminarFila(nhc, pacienteIngresado.getId_i());
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
            Toast.makeText(datos_ingreso.this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }
    }

    private int actualizar_dp(int id) {
        Log.d("NHC", "ID: "+ id);
        Paciente p = obtenerPaciente(id);
        tv_datosubicacion.setText("Numero de cama/Servicio:" + p.getNumero_cama()+ "/"+p.getServicio());
        tv_nhc.setText("NHC: " + p.getNhc());
        tv_name.setText("Nombre: " + p.getNombre() + " " + p.getApellidos());
        tv_genero.setText("Genero: "+p.getGenero());
        tv_edad.setText("Edad: "+p.getEdad());
        tv_nac.setText("Nacionalidad: " + p.getNacionalidad());
        tv_fecnac.setText("Fecha de nacimiento: "+p.getFec_nac());
        // Estado del paciente
        if(p.getEstado().equals("ingreso")) {
            Log.d("PACIENTE", "Ingresado");
            return 0;
        } else {
            Log.d("PACIENTE", "Alta " + p.getEstado());
            return 1;
        }

    }

    private Paciente obtenerPaciente(int id) {
        for(int i = 0;i<lista_pac.size();i++) {
            if(lista_pac.get(i).getNhc() == id) {
                return lista_pac.get(i);
            }
        }
        return null;
    }

    public List<Paciente> cargar_BDPacientes() throws IOException, JSONException {

        InputStream inputStream = getResources().openRawResource(R.raw.bd_pacientes);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }

        String json = text.toString();
        List<Paciente> lista_pacientes = new ArrayList<Paciente>();

        JSONObject object = new JSONObject(json);
        JSONArray json_array = object.getJSONArray("pacientes");

        for (int i = 0; i < json_array.length(); i++) {
            lista_pacientes.add(new Paciente(json_array.getJSONObject(i)));
        }
        return lista_pacientes;
    }

    public List<Ingreso> cargar_ingresos() throws IOException, JSONException {

        InputStream inputStream = getResources().openRawResource(R.raw.ingreso_consulta);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        Log.d("INGRESOS", "Tamos aqui");

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }

        String json = text.toString();
        List<Ingreso> lista_ingresos= new ArrayList<Ingreso>();

        JSONObject object = new JSONObject(json);
        JSONArray json_array = object.getJSONArray("ingresos");

        Log.d("INGRESOS", "json: " + json_array.length());

        for (int i = 0; i < json_array.length(); i++) {
            lista_ingresos.add(new Ingreso(json_array.getJSONObject(i)));
        }
        Log.d("INGRESOS", "lista ingresos: " + lista_ingresos.size());
        return lista_ingresos;
    }

    private  ArrayList<Monitorizacion> cargar_monitorizacion(int nhc) throws JSONException {

        ArrayList<Monitorizacion> v = null;
        try {
            v = monitorizacionSql.obtenerMonitorizacion(nhc);
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
            Toast.makeText(datos_ingreso.this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }
        for(int i = 0; i<v.size();i++) {
            Log.d("DATOS", v.get(i).getNhc() + " " + v.get(i).getTurno() + " " + v.get(i).getT() + " " + v.get(i).getTa() + " " + v.get(i).getPa());
        }
        return v;


    }

    private void update_monitorizacion(int nhc, int hora, int t, int ta, int pa)  {
        try {
            monitorizacionSql.insertarMonitorizacion(nhc, hora, t,ta,pa);
        } catch (ExeceptionPropia execeptionPropia) {
            execeptionPropia.printStackTrace();
            Toast.makeText(datos_ingreso.this, "Error en los datos: id = " + execeptionPropia.getMessage().toString(), Toast.LENGTH_SHORT).show();

        }
        try {
            mon = cargar_monitorizacion(Integer.parseInt(id));
            //monitorizacionSql.eliminarBD();
            //Log.d("MONITORIZACION", "CARGAMOS " + mon.get(Integer.parseInt(id) - 1).getDatos().size());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        drawChart(mon, 0, Integer.parseInt(id));

        //return lista_monitorizacion;


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id_a) {
        Log.d("PULSADO", "HEMOS PULSADO");
        if(listView == parent) {
            if (tipoUser == 2) {
                Log.d("PULSADO", "HEMOS PULSADO 2: " + position);
                Toast.makeText(this, lista_medic_final.get(position).getNombre(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(this, readNfcMed.class);
                i.putExtra("identificador", items_administrar.get(position).getId());
                i.putExtra("lote", items_administrar.get(position).getLote());
                Log.d("COMPATIBILIDAD", "Fecha antes: " + items_administrar.get(position).getFecha_cad() +
                        " ---- " + items_administrar.get(position).getId());
                i.putExtra("fecha_cad", items_administrar.get(position).getFecha_cad());
                // Falta esto
                i.putExtra("id_paciente", Integer.parseInt(id));
                i.putExtra("tipo_usuario", tipoUser);
                int turno = calcularTurno();
                i.putExtra("turno", turno);

                startActivity(i);
            } else {
                Toast.makeText(this,lista_medic_final.get(position).getNombre(), Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onClick(View v) {


        if(v == btn_anadir) {
            /*AlertDialog.Builder dialog = new AlertDialog.Builder(datos_ingreso.this);
            dialog.setTitle("Introduzca datos");
            Context context = datos_ingreso.this;
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);

            final EditText turno = new EditText(context);
            turno.setHint("Hora");
            layout.addView(turno);

            final EditText temperatura = new EditText(context);
            temperatura.setHint("Temperatura");
            layout.addView(temperatura);

            final EditText ta = new EditText(context);
            ta.setHint("Tension arterial");
            layout.addView(ta);

            final EditText p = new EditText(context);
            p.setHint("Pulso");
            layout.addView(p);

            dialog.setView(layout);

            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    Log.d("DIALOG", "T:" + temperatura.getText() + ta.getText() + p.getText());
                    String horai = turno.getText().toString();
                    Log.d("DIALOG", "Hora:" + horai);
                    int t = Integer.parseInt(temperatura.getText().toString());
                    int tai = Integer.parseInt(ta.getText().toString());
                    int pi = Integer.parseInt(p.getText().toString());

                    //drawChart(mon, 0, Integer.parseInt(id));
                    // No hace falta aqui porque ya dibujariamos en update
                    update_monitorizacion(Integer.parseInt(id), Integer.parseInt(horai), t, tai, pi);

                    try {
                        cargar_monitorizacion(Integer.parseInt(id));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });

            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    return;
                }
            });

            dialog.show();*/
            // con este tema personalizado evitamos los bordes por defecto
            customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
            //deshabilitamos el título por defecto
            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //obligamos al usuario a pulsar los botones para cerrarlo
            customDialog.setCancelable(false);
            //establecemos el contenido de nuestro dialog
            customDialog.setContentView(R.layout.dialog_anadir);

            TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
            titulo.setText("AÑADIR MONITORIZACION");

            final EditText pulso = (EditText) customDialog.findViewById(R.id.pulso_a);

            final EditText temp = (EditText) customDialog.findViewById(R.id.temperatura_a);

            final EditText tension = (EditText) customDialog.findViewById(R.id.tensionA_a);

            final EditText hora = (EditText) customDialog.findViewById(R.id.hora_a);


            //TextView contenido = (TextView) customDialog.findViewById(R.id.contenido);
            //contenido.setText("Mensaje con el contenido del dialog");

            ((Button) customDialog.findViewById(R.id.aceptar)).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view)
                {

                    String pulso_s = pulso.getText().toString();
                    String temp_s = temp.getText().toString();
                    String ten_s = tension.getText().toString();
                    String hora_s = hora.getText().toString();


                    if(pulso_s.length()>0 && temp_s.length()>0 && ten_s.length()>0
                            && hora_s.length()>0) {
                        update_monitorizacion(Integer.parseInt(id), Integer.parseInt(hora_s), Integer.parseInt(temp_s)
                                , Integer.parseInt(ten_s), Integer.parseInt(pulso_s));

                        try {
                            cargar_monitorizacion(Integer.parseInt(id));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        customDialog.dismiss();
                    } else {
                        Toast.makeText(datos_ingreso.this, "ERROR EN LOS DATOS", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            ((Button) customDialog.findViewById(R.id.cancelar)).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view)
                {
                    customDialog.dismiss();
                    Toast.makeText(datos_ingreso.this, "CANCELAR", Toast.LENGTH_SHORT).show();

                }
            });

            customDialog.show();

        }

        if(v == tv_hist) {
            Log.d("HISTORIAL", "Hemos pulsado el historial");
            Intent i = new Intent(this, historialMedico_activity.class);
            i.putExtra("nhc", Integer.parseInt(id));
            startActivity(i);
        }

    }



    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(c_t == buttonView) {
            Log.d("CHECK", "Pulsamos");
            if(isChecked) {
                drawChart(mon, 0, Integer.parseInt(id));
            } else {
                drawChart(mon, 1, Integer.parseInt(id));
            }
        }
        if(c_pa == buttonView) {
            Log.d("CHECK", "Pulsamos");
            if(isChecked) {
                drawChart(mon, 0, Integer.parseInt(id));
            } else {
                drawChart(mon, 3, Integer.parseInt(id));
            }
        }
        if(c_ta == buttonView) {
            Log.d("CHECK", "Pulsamos");
            if(isChecked) {
                drawChart(mon, 0, Integer.parseInt(id));
            } else {
                drawChart(mon, 2, Integer.parseInt(id));
            }
        }
    }
}
