package tfg.es.udc.es.tfgapp;

/**
 * Created by Juan Manuel on 06/02/2017.
 */


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MobileArrayAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public MobileArrayAdapter(Context context, String[] values) {
        super(context, R.layout.example_lv, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.example_lv, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
        textView.setText(values[position]);

        // Change icon based on name
        String s = values[position];

        System.out.println(s);

        if (s.equals("WindowsMobile")) {
            imageView.setImageResource(R.drawable.nfc);
        } else if (s.equals("iOS")) {
            imageView.setImageResource(R.drawable.nfc_med);
        } else if (s.equals("Blackberry")) {
            imageView.setImageResource(R.drawable.add);
        } else {
            imageView.setImageResource(R.drawable.cruz);
        }
        Log.d("LISTVIEW", "Pasamos pora aqui");

        return rowView;
    }
}