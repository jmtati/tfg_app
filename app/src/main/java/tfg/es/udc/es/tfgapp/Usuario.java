package tfg.es.udc.es.tfgapp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Juan Manuel on 20/02/2016.
 */
public class Usuario {

    public String name;
    public String password;
    public enum TipoUser
    {
        MEDICO, ENFERMERA, AUXILIAR
    };
    TipoUser tipo;

    public Usuario(JSONObject objetoJSON) throws JSONException {
        name = objetoJSON.getString("usuario");
        password = objetoJSON.getString("password");
        if(objetoJSON.getString("tipo").equals("medico")) {
            tipo = TipoUser.MEDICO;
        } else {
            if(objetoJSON.getString("tipo").equals("enfermera")) {
                tipo = TipoUser.ENFERMERA;
            } else {
                tipo = TipoUser.AUXILIAR;
            }
        }
    }
}
