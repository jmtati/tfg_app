package tfg.es.udc.es.tfgapp;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan Manuel on 06/02/2017.
 */
public class Metodos extends Application {

    public List<Ingreso> cargar_ingresos(Context context) throws IOException, JSONException {

        InputStream inputStream = context.getResources().openRawResource(R.raw.ingreso_consulta);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        Log.d("INGRESOS", "Tamos aqui");

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }

        String json = text.toString();
        List<Ingreso> lista_ingresos= new ArrayList<Ingreso>();

        JSONObject object = new JSONObject(json);
        JSONArray json_array = object.getJSONArray("ingresos");

        Log.d("INGRESOS", "json: " + json_array.length());

        for (int i = 0; i < json_array.length(); i++) {
            lista_ingresos.add(new Ingreso(json_array.getJSONObject(i)));
        }
        Log.d("INGRESOS", "lista ingresos: " + lista_ingresos.size());
        return lista_ingresos;
    }

    public List<Paciente> cargar_BDPacientes() throws IOException, JSONException {

        InputStream inputStream = getResources().openRawResource(R.raw.bd_pacientes);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }

        String json = text.toString();
        List<Paciente> lista_pacientes = new ArrayList<Paciente>();

        JSONObject object = new JSONObject(json);
        JSONArray json_array = object.getJSONArray("pacientes");

        for (int i = 0; i < json_array.length(); i++) {
            lista_pacientes.add(new Paciente(json_array.getJSONObject(i)));
        }
        return lista_pacientes;
    }

    public Ingreso buscarIngreso(int nhc, List<Ingreso> lista_ingre) {
        for(int i = 0;i<lista_ingre.size();i++) {
            if(lista_ingre.get(i).getId() == nhc) {
                return lista_ingre.get(i);
            }
        }
        return null;

    }


}
