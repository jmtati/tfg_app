package tfg.es.udc.es.tfgapp;

/**
 * Created by Juan Manuel on 26/04/2016.
 */
public class Medicamente_lista {

    private String nombre;
    private int tomada = 0;

    public Medicamente_lista() {
        super();
    }

    public Medicamente_lista(String nombre, int tomada) {
        super();
        this.nombre = nombre;
        this.tomada = tomada;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTomada(int tomada) {
        this.tomada = tomada;
    }

    public String getNombre() {

        return nombre;
    }
    public int getTomada() {
        return this.tomada;
    }
}
