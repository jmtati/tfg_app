package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Vector;

/**
 * Created by Juan Manuel on 19/07/2016.
 */

public class HistorialMedicoSQLite extends SQLiteOpenHelper {

    //Métodos de SQLiteOpenHelper
    public HistorialMedicoSQLite(Context context) {
        super(context, "historial_medico", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE historial_medico (" +
                "nhc INTEGER, especialidad TEXT, fecha VARCHAR, id_ii INTEGER, " +
                "id_ih INTEGER, id_ia INTEGER)");
        Log.d("HISMEDICO", " creamos BDHISTORIAL");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertarHistorialMedico(int nhc, String especialidad, String fecha, int i, int h, int a) throws ExeceptionPropia {
        if(nhc > 0 && i > 0 && h >0 && a>0) {

            if(comprobacion_fecha(fecha) == 1) {
                SQLiteDatabase db = getWritableDatabase();
                db.execSQL("INSERT INTO historial_medico VALUES ( " + nhc + ", '" + especialidad +
                        "', '" + fecha + "', " + i + ", " + h + ", " + a + " )");
                Log.d("HISMEDICO", "INSERTAMOS BDHISTORIAL");
                db.close();
            } else {
                throw new ExeceptionPropia("2");
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtener(int nhc) throws  ExeceptionPropia {
        if(nhc>0) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT nhc, especialidad, fecha, id_ii, id_ih, id_ia FROM " +
                    "historial_medico WHERE nhc = " + nhc + " ORDER BY fecha DESC", null);
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getString(1) + "," + cursor.getString(2) + "," + cursor.getString(3) + "," + cursor.getString(4) + "," + cursor.getString(5));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw new ExeceptionPropia("1");
        }
    }


    public void eliminarBD() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("historial_medico", null, null);

    }

    private int comprobacion_fecha(String fecha_fin) {
        String[] c = fecha_fin.split("-");
        if(c.length==3) {
            int anho = Integer.parseInt(c[0].toString());
            int mes = Integer.parseInt(c[1].toString());
            int dia = Integer.parseInt(c[2].toString());
            Log.d("FECHAALTA", "1: " + anho + "," + mes + "," + dia);


            if (anho > 0 && mes > 0 && mes < 13 && dia > 0 && dia < 31) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }
}
