package tfg.es.udc.es.tfgapp;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by Juan Manuel on 13/12/2016.
 */
public class HistorialDatos extends ActionBarActivity {
    int nhc;
    String especialidad;
    int id_i, id_h, id_a;
    String fecha;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public HistorialDatos(int nhc, String especialidad, String fecha, int id_i, int id_h, int id_a) {
        this.nhc = nhc;
        this.especialidad = especialidad;
        this.fecha = fecha;

        this.id_i = id_i;
        this.id_h = id_h;
        this.id_a = id_a;
    }

    public int getNhc() {
        return nhc;
    }

    public void setNhc(int nhc) {
        this.nhc = nhc;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public int getId_i() {
        return id_i;
    }

    public void setId_i(int id_i) {
        this.id_i = id_i;
    }

    public int getId_h() {
        return id_h;
    }

    public void setId_h(int id_h) {
        this.id_h = id_h;
    }

    public int getId_a() {
        return id_a;
    }

    public void setId_a(int id_a) {
        this.id_a = id_a;
    }
}
