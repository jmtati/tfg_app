package tfg.es.udc.es.tfgapp;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    Button iniciosesion;
    Button btnNfc;
    EditText user;
    EditText pass;
    private NfcAdapter mNfcAdapter;
    List<Usuario> lista_users = new ArrayList<Usuario>();
    Tag detectedTag;
    TextView txtType,txtSize,txtWrite,txtRead;
    NfcAdapter nfcAdapter;
    IntentFilter[] readTagFilters;
    PendingIntent pendingIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            lista_users = cargar_BD();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("baseDatos", "Tamano "+ lista_users.size());
        setContentView(R.layout.activity_main);
        iniciosesion = ((Button) findViewById(R.id.aceccs));
        iniciosesion.setOnClickListener(this);
        btnNfc = (Button) findViewById(R.id.btn_nfc);
        btnNfc.setOnClickListener(this);
        user = ((EditText) findViewById(R.id.userid));
        pass = ((EditText) findViewById(R.id.pass));

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if(v == iniciosesion) {
            //Toast.makeText(this, "PULSADO INICIO SESION", Toast.LENGTH_SHORT).show();
            String userid = ((EditText) findViewById(R.id.userid)).getText().toString();
            String passid = ((EditText) findViewById(R.id.pass)).getText().toString();
            if(userid.length()>0 && passid.length()>0) {
                acceso(userid, passid);
            } else {
                Toast.makeText(MainActivity.this, "ERROR EN LOS DATOS", Toast.LENGTH_SHORT).show();
            }
        }
        if(v == btnNfc) {
            // Dialogo para informar que acerque etiqueta NFC
            if(comprobamos_nfc()==1) {
                Log.d("NFCINICIO", "Entramos aqui");
                // Dialogo para informar que acerque etiqueta NFC
                FragmentManager fragmentManager = getSupportFragmentManager();
                DialogoAlerta dialogo = new DialogoAlerta();
                dialogo.show(fragmentManager, "tag");

                nfcAdapter = NfcAdapter.getDefaultAdapter(this);
                detectedTag =getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);

                pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                        new Intent(this, getClass()).
                                addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

                IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
                IntentFilter filter2     = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
                readTagFilters = new IntentFilter[]{tagDetected,filter2};
                nfcAdapter.enableForegroundDispatch(this, pendingIntent, readTagFilters, null);
                Log.d("NFCINICIO", "Llegamos aqui");

            } else {
                Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            }

        }
    }


    public void acceso(String userid, String passid) {
        int tipo_user = 0;
        if(comprobar_acceso(userid, passid)) {
            Usuario user = obtenerUser(userid);
            Intent intent = new Intent(this, menu_app.class);
            if(user.tipo == Usuario.TipoUser.MEDICO) {
                tipo_user = 3;
            } else {
                if(user.tipo == Usuario.TipoUser.ENFERMERA) {
                    tipo_user = 2;
                } else {
                    tipo_user = 1;
                }
            }
            intent.putExtra("tipo_usuario", tipo_user);
            startActivity(intent);
            finish();
        } else {
            // Lanzamos excepcion
            Toast.makeText(this, "User/Pass no válido.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {

        super.onResume();
        //nfcAdapter.enableForegroundDispatch(this, pendingIntent, readTagFilters, null);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Boolean comprobar_acceso(String user, String password) {
        int i = 0;
        while(i < lista_users.size()) {
            Log.d("Usuarios", "User "+lista_users.get(i).name + " Pass "+ lista_users.get(i).password);
            if(lista_users.get(i).name.equals(user) && lista_users.get(i).password.equals(password)) {
                return true;

            }
            i++;
        }
        return false;
    }

    private Usuario obtenerUser(String user) {
        int i = 0;
        while(i < lista_users.size()) {
            Log.d("Usuarios", "User "+lista_users.get(i).name + " Pass "+ lista_users.get(i).password);
            if(lista_users.get(i).name.equals(user)) {
                return lista_users.get(i);

            }
            i++;
        }
        return null;
    }

    private List<Usuario> cargar_BD() throws IOException, JSONException {

        InputStream inputStream = getResources().openRawResource(R.raw.basedatos);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }

        String json = text.toString();
        List<Usuario> lista_usuarios = new ArrayList<Usuario>();

        JSONObject object = new JSONObject(json);
        JSONArray json_array = object.getJSONArray("usuarios");

        for (int i = 0; i < json_array.length(); i++) {
            lista_usuarios.add(new Usuario(json_array.getJSONObject(i)));
        }
        return lista_usuarios;
    }

    private int comprobamos_nfc() {
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (mNfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
            return 0;

        }

        if (!mNfcAdapter.isEnabled()) {
            Toast.makeText(this, "NFC is disabled.", Toast.LENGTH_SHORT).show();
            // Lanzariamos pantalla para conectar NFC
            return 1;
        } else {
            Toast.makeText(this, "Utilizaremos NFC.", Toast.LENGTH_SHORT).show();
            return 1;
        }
    }


    protected void onNewIntent(Intent intent) {
        Log.d("NFCINICIO", "Entramos para leer onNewIntent");

        setIntent(intent);
        if(getIntent().getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)){
            detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            readFromTag(getIntent());

        }
    }

    public void readFromTag(Intent intent){

        Log.d("NFCINICIO", "Entramos para leer");

        Ndef ndef = Ndef.get(detectedTag);

        try{
            ndef.connect();
            String text = null;

            // txtType.setText(ndef.getType().toString());
            // txtSize.setText(String.valueOf(ndef.getMaxSize()));
            // txtWrite.setText(ndef.isWritable() ? "True" : "False");
            Parcelable[] messages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            if (messages != null) {
                NdefMessage[] ndefMessages = new NdefMessage[messages.length];
                for (int i = 0; i < messages.length; i++) {
                    ndefMessages[i] = (NdefMessage) messages[i];
                }
                NdefRecord record = ndefMessages[0].getRecords()[0];

                byte[] payload = record.getPayload();
                text = new String(payload);
                // txtRead.setText(text);


                ndef.close();

            }
            String[] user = text.split("-");
            String userid = user[0];
            String pass = user[1];
            Log.d("NFCTATI", "Texto: " + userid + " " + pass);
            acceso(userid, pass);

        }
        catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Cannot Read From Tag.", Toast.LENGTH_LONG).show();
        }
    }



}



