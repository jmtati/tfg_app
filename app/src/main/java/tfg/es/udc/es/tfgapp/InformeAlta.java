package tfg.es.udc.es.tfgapp;

/**
 * Created by Juan Manuel on 05/08/2016.
 */
public class InformeAlta {

    int nhc, pa, t, ta;
    String fecha_salida, fecha_entrada, enfermedad, recomendaciones;

    public InformeAlta(int nhc,  String fecha_salida, String fecha_entrada
                       , String recomendaciones, int pa, int t, int ta) {

        this.nhc = nhc;
        this.fecha_salida = fecha_salida;
        this.fecha_entrada = fecha_entrada;
        this.recomendaciones = recomendaciones;
        this.pa = pa;
        this.t = t;
        this.ta = ta;
    }

    public int getNhc() {
        return nhc;
    }

    public void setNhc(int nhc) {
        this.nhc = nhc;
    }

    public int getPa() {
        return pa;
    }

    public void setPa(int pa) {
        this.pa = pa;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public int getTa() {
        return ta;
    }

    public void setTa(int ta) {
        this.ta = ta;
    }

    public String getFecha_salida() {
        return fecha_salida;
    }

    public void setFecha_salida(String fecha_salida) {
        this.fecha_salida = fecha_salida;
    }

    public String getFecha_entrada() {
        return fecha_entrada;
    }

    public void setFecha_entrada(String fecha_entrada) {
        this.fecha_entrada = fecha_entrada;
    }

    public String getRecomendaciones() {
        return recomendaciones;
    }

    public void setRecomendaciones(String recomendaciones) {
        this.recomendaciones = recomendaciones;
    }
}
