package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by Juan Manuel on 19/07/2016.
 */

public class HistorialSQLite extends SQLiteOpenHelper {
    //Métodos de SQLiteOpenHelper
    public HistorialSQLite(Context context) {
        super(context, "historial", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE historial (" +
                "id_h INTEGER, nhc INTEGER, fecha_entrada VARCHAR, " +
                "id_medicamento INTEGER, pautas VARCHAR, fecha_inicio VARCHAR, fecha_fin VARCHAR," +
                " observacions VARCHAR, tipo VARCHAR, dosis DOUBLE, vias VARCHAR)");
        Log.d("CLINICO", "creamos bd historial ingreso");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Vector obtenerMedicacion(int nhc, int id_med, String fecha_entrada) throws ExeceptionPropia {

        if (nhc > 0 && id_med > 0) {
            if (comprobacion_fecha(fecha_entrada) == 1) {
                Vector result = new Vector();
                SQLiteDatabase db = getReadableDatabase();
                Cursor cursor = db.rawQuery("SELECT id_medicamento, pautas, fecha_inicio, fecha_fin, observacions, tipo FROM " +
                        "historial WHERE id_h = " + nhc + " AND id_medicamento = " + id_med + " AND fecha_entrada = '" + fecha_entrada + "'", null);
                while (cursor.moveToNext()) {
                    result.add(cursor.getInt(0) + "," + cursor.getString(1) + "," + cursor.getString(2) + "," + cursor.getString(3)
                            + "," + cursor.getString(4) + "," + cursor.getString(5));
                }
                cursor.close();
                db.close();
                return result;
            } else {
                throw new ExeceptionPropia("2");
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtenerHistorial(int id_h, int nhc, String fecha_entrada) throws ExeceptionPropia {

        if (nhc > 0 && id_h > 0) {

                Vector result = new Vector();
                SQLiteDatabase db = getReadableDatabase();
                Cursor cursor;
                if (fecha_entrada.equals("todos")) {
                    cursor = db.rawQuery("SELECT id_medicamento, pautas, fecha_inicio, fecha_fin, observacions, dosis, vias FROM " +
                            "historial WHERE id_h = " + id_h + " AND nhc = " + nhc, null);
                } else {
                    if (comprobacion_fecha(fecha_entrada) == 1) {
                        cursor = db.rawQuery("SELECT id_medicamento, pautas, fecha_inicio, fecha_fin, observacions, dosis, vias FROM " +
                                "historial WHERE id_h = " + id_h + " AND nhc = " + nhc + " AND fecha_entrada = '" + fecha_entrada + "'", null);

                    } else {
                        throw new ExeceptionPropia("2");
                    }
                }
                while (cursor.moveToNext()) {
                    result.add(cursor.getInt(0) + "," + cursor.getString(1) + "," + cursor.getString(2) + "," + cursor.getString(3) + "," + cursor.getString(4)
                            + "," + cursor.getDouble(5) + "," + cursor.getString(6));
                }
                cursor.close();
                db.close();
                return result;

        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtenerMedicacionAlta(int nhc) throws ExeceptionPropia {

        if (nhc > 0) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT id_medicamento, pautas, fecha_inicio, fecha_fin, observacions, tipo FROM " +
                    "historial WHERE nhc = " + nhc + "  AND tipo = 'alta'", null);
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getString(1) + "," + cursor.getString(2) + "," + cursor.getString(3)
                        + "," + cursor.getString(4) + "," + cursor.getString(5));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public void insertarHistorial(int id_h, int nhc, String fecha_entrada, int id_med,
                                  String pautas, String fecha_inicio, String fecha_fin,
                                  String observaciones, String tipo, double dosis, String vias) throws ExeceptionPropia {

        if (id_h > 0 && nhc > 0 && id_med > 0 && dosis > -1) {
            if (comprobacion_fecha(fecha_entrada) == 1 && comprobacion_fecha(fecha_inicio) == 1
                    && comprobacion_fecha(fecha_fin) == 1) {
                if (tipo.toLowerCase().equals("ingreso") || tipo.toLowerCase().equals("alta")) {
                    if (comprobacionVias(vias)) {
                        if(comprobacion_pautas(pautas) == 1) {
                            SQLiteDatabase db = getWritableDatabase();
                            db.execSQL("INSERT INTO historial VALUES ( " + id_h + ", " + nhc + ", '" + fecha_entrada + "', " +
                                    id_med + ", '" + pautas + "', '" + fecha_inicio + "', '" + fecha_fin + "', '" + observaciones
                                    + "', '" + tipo + "', " + dosis + ", '" + vias + "' )");
                            db.close();
                            Log.d("CLINICO", "Insertando BD CLINICA:" + id_h);
                        } else {
                            throw new ExeceptionPropia("7");
                        }
                    } else {
                        throw new ExeceptionPropia("5");
                    }
                } else {
                    throw new ExeceptionPropia("4");
                }
            } else {
                throw new ExeceptionPropia("2");
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public void eliminarDatos() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM historial");
    }

    public void eliminarBD() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS historial");
        //db.delete("historial",null,null);
        Log.d("CLINICO", "Eliminando BD CLINICA");

    }

    private int comprobacion_pautas(String pautas) {
        String[] c = pautas.split("-");
        if(c.length==3) {
            return 1;
        } else {
            return 0;
        }
    }

    private int comprobacion_fecha(String fecha_fin) {
        String[] c = fecha_fin.split("-");
        if(c.length==3) {
            int anho = Integer.parseInt(c[0].toString());
            int mes = Integer.parseInt(c[1].toString());
            int dia = Integer.parseInt(c[2].toString());
            Log.d("FECHAALTA", "1: " + anho + "," + mes + "," + dia);


            if (anho > 0 && mes > 0 && mes < 13 && dia > 0 && dia < 31) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }

    private boolean comprobacionVias(String vias) {
        if (vias.toLowerCase().equals("oral")) {
            return true;
        } else {
            if (vias.toLowerCase().equals("intravenosa")) {
                return true;
            } else {
                if (vias.toLowerCase().equals("subcutanea")) {
                    return true;
                } else {
                    if (vias.toLowerCase().equals("intramuscular")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    }
}
