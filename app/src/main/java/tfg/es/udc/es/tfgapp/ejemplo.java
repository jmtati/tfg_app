package tfg.es.udc.es.tfgapp;

import android.app.ListActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;


public class ejemplo extends ActionBarActivity {

    static final String[] MOBILE_OS =
            new String[] { "Android", "iOS", "WindowsMobile", "Blackberry"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejemplo);
        ListView lv = (ListView) findViewById(R.id.lv_ejemplo);
        //setListAdapter(new MobileArrayAdapter(this, MOBILE_OS));
        lv.setAdapter(new MobileArrayAdapter(this, MOBILE_OS));

    }



}
