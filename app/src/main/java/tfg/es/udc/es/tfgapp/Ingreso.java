package tfg.es.udc.es.tfgapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan Manuel on 14/03/2016.
 */
public class Ingreso {
    int id;
    ArrayList<Medicacion> lista_medicacion;
    String data, hora, pro, diag, tlfcontacto, centro, probas;
    JSONArray cte, di, dp, pa, al;
    List<String> alerxias, probasaportadas_lista;
    int ta;
    int fc;
    int t;
    int fr;
    int peso;
    int id_i;

    public int getId_i() {
        return id_i;
    }

    public void setId_i(int id_i) {
        this.id_i = id_i;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getPro() {
        return pro;
    }

    public void setPro(String pro) {
        this.pro = pro;
    }

    public String getDiag() {
        return diag;
    }

    public void setDiag(String diag) {
        this.diag = diag;
    }

    public JSONArray getCte() {
        return cte;
    }

    public void setCte(JSONArray cte) {
        this.cte = cte;
    }

    public int getTa() {
        return ta;
    }

    public void setTa(int ta) {
        this.ta = ta;
    }

    public int getFc() {
        return fc;
    }

    public void setFc(int fc) {
        this.fc = fc;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public int getFr() {
        return fr;
    }

    public void setFr(int fr) {
        this.fr = fr;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public int getTalla() {

        return talla;
    }

    public void setTalla(int talla) {
        this.talla = talla;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Medicacion> getLista_medicacion() {
        return lista_medicacion;
    }

    public void setLista_medicacion(ArrayList<Medicacion> lista_medicacion) {
        this.lista_medicacion = lista_medicacion;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTlfcontacto() {
        return tlfcontacto;
    }

    public void setTlfcontacto(String tlfcontacto) {
        this.tlfcontacto = tlfcontacto;
    }

    public String getCentro() {
        return centro;
    }

    public void setCentro(String centro) {
        this.centro = centro;
    }

    int talla;

    public String getProbas() {
        return probas;
    }

    public void setProbas(String probas) {
        this.probas = probas;
    }

    public List<String> getAlerxias() {
        return alerxias;
    }

    public void setAlerxias(List<String> alerxias) {
        this.alerxias = alerxias;
    }

    public List<String> getProbasaportadas_lista() {
        return probasaportadas_lista;
    }

    public void setProbasaportadas_lista(List<String> probasaportadas_lista) {
        this.probasaportadas_lista = probasaportadas_lista;
    }

    public Ingreso(JSONObject objetoJSON) throws JSONException {
        di = objetoJSON.getJSONArray("datosdeingreso");
        id =  di.getJSONObject(0).getInt("nhc");
        id_i = di.getJSONObject(0).getInt("id_i");
        data =  di.getJSONObject(0).getString("fecha");
        hora =  di.getJSONObject(0).getString("hora");
        pro =  di.getJSONObject(0).getString("procedencia");
        diag =  di.getJSONObject(0).getString("diagnostico");
        cte =  di.getJSONObject(0).getJSONArray("constantes");
        ta = cte.getJSONObject(0).getInt("ta");
        fc = cte.getJSONObject(0).getInt("fc");
        t = cte.getJSONObject(0).getInt("t");
        fr = cte.getJSONObject(0).getInt("fr");
        peso = cte.getJSONObject(0).getInt("peso");
        talla = cte.getJSONObject(0).getInt("talla");
        dp = objetoJSON.getJSONArray("datospersonales");
        tlfcontacto = dp.getJSONObject(0).getString("tlfcontacto");
        centro =  dp.getJSONObject(0).getString("centroSalud");
        pa = objetoJSON.getJSONArray("probasaportadas");
        probasaportadas_lista = new ArrayList<String>();
        for(int h=0;h<pa.length();h++) {
            probasaportadas_lista.add(pa.get(h).toString());

        }
        al = objetoJSON.getJSONArray("alerxias");
        alerxias = new ArrayList<String>();
        for(int j = 0; j<al.length(); j++) {
            Log.d("ALERXIAS", "alerxia: " + al.get(j).toString());
            alerxias.add(al.get(j).toString());

        }
        /*****************************/

        JSONArray json_array = objetoJSON.getJSONArray("medicacion/sueros");
        Log.d("Tamanos dentro", "ID:" + json_array.length());
        lista_medicacion = new ArrayList<Medicacion>();
        for (int i = 0; i < json_array.length(); i++) {
            lista_medicacion.add(new Medicacion(json_array.getJSONObject(i)));
        }
        Log.d("Lista medicacion", "Tamano" + lista_medicacion.size());
    }


}
