package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Vector;

/**
 * Created by Juan Manuel on 19/07/2016.
 */

public class InfoAltaSQLite extends SQLiteOpenHelper {
    //Métodos de SQLiteOpenHelper
    public InfoAltaSQLite(Context context) {
        super(context, "infoaltas", null, 1);
    }

    @Override public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE altas ("+
                "id_a INTEGER PRIMARY KEY AUTOINCREMENT, nhc INTEGER, "+
                "fecha_entrada VARCHAR, fecha_salida VARCHAR, " +
                "recomendaciones VARCHAR, ta INTEGER, t INTEGER, pa INTEGER )");
        Log.d("INFOALTA", "creamos bd info alta");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int insertarAlta(InformeAlta alta) throws ExeceptionPropia{
        if(alta.getNhc() >0 ) {
            if(comprobacion_fecha(alta.getFecha_entrada()) == 1 && comprobacion_fecha(alta.getFecha_salida()) == 1) {
                SQLiteDatabase db = getWritableDatabase();
                db.execSQL("INSERT INTO altas VALUES ( null, " + alta.getNhc() + ", '" + alta.getFecha_entrada() + "', '" + alta.getFecha_salida() + "', '" + alta.getRecomendaciones() + "', " + alta.getTa() +
                        ", " + alta.getT() + ", " + alta.getPa()
                        + ")");
                SQLiteDatabase db2 = getReadableDatabase();
                Cursor c = db2.rawQuery("select last_insert_rowid()", null);
                int auto = -1;
                while (c.moveToNext()) {
                    Log.d("INFOALTA", "Introducido informe de alta: " + c.getString(0));
                    auto = Integer.parseInt(c.getString(0));
                }

                db2.close();
                db.close();
                return auto;
            } else {
                throw new ExeceptionPropia("2");
            }
        } else {
            throw new ExeceptionPropia("1");
        }

    }

    public Vector obtenerAlta(int id, int nhc) throws ExeceptionPropia {
        if(id >0 && nhc >0) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT fecha_entrada, fecha_salida, recomendaciones, ta, t, pa FROM " +
                    "altas WHERE id_a = " + id + " AND nhc = " + nhc, null);
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getInt(1) + "," + cursor.getInt(2) + "," + cursor.getInt(3) + "," + cursor.getInt(4) + ","
                        + cursor.getInt(5));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw new ExeceptionPropia("1");
        }
    }


    public void eliminarBD() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS altas");
        Log.d("INFOALTA", "Eliminamos BD");
        //db.delete("infoaltas",null,null);

    }

    private int comprobacion_fecha(String fecha_fin) {
        String[] c = fecha_fin.split("-");
        if(c.length==3) {
            int anho = Integer.parseInt(c[0].toString());
            int mes = Integer.parseInt(c[1].toString());
            int dia = Integer.parseInt(c[2].toString());
            Log.d("FECHAALTA", "1: " + anho + "," + mes + "," + dia);


            if (anho > 0 && mes > 0 && mes < 13 && dia > 0 && dia < 31) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }
}
