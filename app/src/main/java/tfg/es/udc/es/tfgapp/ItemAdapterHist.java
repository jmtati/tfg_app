package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Juan Manuel on 26/04/2016.
 */
public class ItemAdapterHist extends BaseAdapter {

    private Context context;
    private List<historialMedico_activity.Datos> items;
    int tipoUser;
    String id_paciente;


    public ItemAdapterHist(Context context, List<historialMedico_activity.Datos> items) {
        this.context = context;
        this.items = items;

    }


    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_hist, parent, false);
        }

        // Set data into the view.
        //CheckBox tvTitle = (CheckBox) rowView.findViewById(R.id.checkbox_tomado);
        TextView tvFecha = (TextView) rowView.findViewById(R.id.fecha_h);
        TextView tvEspecialidad = (TextView) rowView.findViewById(R.id.especialidad_h);

        String fecha = this.items.get(position).getFecha();
        String especialidad = this.items.get(position).getEspecialidad();

        tvFecha.setText(fecha);
        tvEspecialidad.setText(especialidad);
        return rowView;
    }


}
