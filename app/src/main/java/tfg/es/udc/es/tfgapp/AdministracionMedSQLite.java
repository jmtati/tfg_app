package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;

/**
 * Created by Juan Manuel on 23/11/2016.
 */
public class AdministracionMedSQLite extends SQLiteOpenHelper {
    //Métodos de SQLiteOpenHelper
    public AdministracionMedSQLite(Context context) {
        super(context, "administracion", null, 1);
    }

    @Override public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE administracion ("+
                "nhc INTEGER, id_m INTEGER, turno INTEGER, administrado INTEGER, lote INTEGER, fecha VARCHAR )");
        Log.d("PUNTUACIONES", "creamos bd ADMINISTRADO MED");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertarAdmin(int nhc, int id_m, int turno, int lote, String fecha) throws ExeceptionPropia{

        if(nhc > 0 && lote > 0 && id_m > 0) {
            String[] c = fecha.split("-");
            if(c.length==3) {
                if(comprobacion_fecha(c) == 1) {
                    SQLiteDatabase db = getWritableDatabase();
                    db.execSQL("INSERT INTO administracion VALUES ( " + nhc + " ,+ " + id_m + " ," + turno + " ," + 1 + ", " + lote +
                            ", '" + fecha + "' )");
                    db.close();
                }
            } else {
                throw new ExeceptionPropia("2");
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtener(int nhc, int turno) throws ExeceptionPropia{
        if(nhc > 0) {
            if(turno == 1 || turno == 2 || turno == 3) {
                Vector result = new Vector();
                SQLiteDatabase db = getReadableDatabase();
                Cursor cursor = db.rawQuery("SELECT id_m FROM " +
                        "administracion WHERE nhc = " + nhc + " AND turno = " + turno, null);
                while (cursor.moveToNext()) {
                    result.add(cursor.getInt(0));
                }
                cursor.close();
                db.close();
                return result;
            } else {
                throw new ExeceptionPropia("3");
            }
        } else {
            throw new ExeceptionPropia("1");
        }
    }

    public Vector obtenerLote(int id_m, int lote) throws ExeceptionPropia{

        if(id_m > 0 && lote > 0) {
            Vector result = new Vector();
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT nhc, fecha FROM " +
                    "administracion WHERE id_m = " + id_m + " AND lote = " + lote, null);
            while (cursor.moveToNext()) {
                result.add(cursor.getInt(0) + "," + cursor.getString(1));
            }
            cursor.close();
            db.close();
            return result;
        } else {
            throw new ExeceptionPropia("1");
        }
    }




    public void eliminarBD() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("administracion",null,null);

    }

    private int comprobacion_fecha(String[] fecha_fin) {
        int anho = Integer.parseInt(fecha_fin[0].toString());
        int mes = Integer.parseInt(fecha_fin[1].toString());
        int dia = Integer.parseInt(fecha_fin[2].toString());
        Log.d("FECHAALTA", "1: " + anho +","+mes+","+dia);


        if(anho > 0 && mes > 0 && mes < 13 && dia >0 && dia < 31) {
            return 1;
        } else {
            return 0;
        }

    }
}
