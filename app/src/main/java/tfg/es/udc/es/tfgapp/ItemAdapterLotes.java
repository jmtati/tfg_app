package tfg.es.udc.es.tfgapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Juan Manuel on 06/02/2017.
 */
public class ItemAdapterLotes extends BaseAdapter {

    private Context context;
    private List<Lotesconpacientes.Datos> items;
    int tipoUser;
    String id_paciente;


    public ItemAdapterLotes(Context context, List<Lotesconpacientes.Datos> items) {
        this.context = context;
        this.items = items;

    }


    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_lotes, parent, false);
        }

        // Set data into the view.
        //CheckBox tvTitle = (CheckBox) rowView.findViewById(R.id.checkbox_tomado);
        TextView tvNhc = (TextView) rowView.findViewById(R.id.nhc_lotes);
        TextView tvFecha = (TextView) rowView.findViewById(R.id.fecha_lotes);

        String nhc = this.items.get(position).getNhc();
        String fecha = this.items.get(position).getFecha();

        tvFecha.setText(fecha);
        tvNhc.setText(nhc);
        return rowView;
    }


}
